#include "cApp.h"

wxIMPLEMENT_APP(cApp);

cApp::cApp() {

}
cApp::~cApp() {

}
bool cApp::OnInit() {
	
	m_frame1 = new VogelQuietsch(nullptr, wxID_ANY, wxT("VogelQuietsch-Quizz"),  wxPoint(0,0), wxSize(1400, 800), wxDEFAULT_FRAME_STYLE);
	wxSize displaySize = wxGetDisplaySize();
	wxSize mainWindowSize = m_frame1->GetSize();
	m_frame1->SetPosition(wxPoint(0, displaySize.GetHeight() - mainWindowSize.GetHeight()));
	m_frame1 -> Show();

	return true;
}
