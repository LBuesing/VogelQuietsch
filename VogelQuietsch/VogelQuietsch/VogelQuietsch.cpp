#include "VogelQuietsch.h"





void VogelQuietsch::bindGuiElements()
{
	m_button_SearchXenoCantoDb->Bind(wxEVT_BUTTON, &VogelQuietsch::searchXenoCanto,this);
	m_button_LoadList->Bind(wxEVT_BUTTON, &VogelQuietsch::onLoadBirdListToInactive, this);
	m_button_LoadListEdit->Bind(wxEVT_BUTTON, &VogelQuietsch::onLoadBirdListToEdit, this);
	m_button_SaveListEdit->Bind(wxEVT_BUTTON, &VogelQuietsch::onSaveBirdListFromEdit, this);
	MyTextDropTarget* mdt = new MyTextDropTarget(m_listBox_LoadedList);
	m_listBox_LoadedList->SetDropTarget(mdt);
	Connect(m_listBox_SearchResults->GetId(), wxEVT_COMMAND_LIST_BEGIN_DRAG, wxListEventHandler(VogelQuietsch::onDragInitSearchToEdit));
	Connect(m_listBox_SearchResults->GetId(), wxEVT_LIST_ITEM_SELECTED, wxListEventHandler(VogelQuietsch::onSelectSearchToEdit));
	Connect(m_listBox_SearchResults->GetId(), wxEVT_LIST_ITEM_DESELECTED, wxListEventHandler(VogelQuietsch::onDeselectSearchToEdit));

	MyTextDropTargetActiveInactive* mdt_active = new MyTextDropTargetActiveInactive(m_listBox_ActiveList,m_listBox_InactiveList);
	m_listBox_ActiveList->SetDropTarget(mdt_active);
	Connect(m_listBox_InactiveList->GetId(), wxEVT_COMMAND_LIST_BEGIN_DRAG, wxListEventHandler(VogelQuietsch::onDragInitInactive));
	Connect(m_listBox_InactiveList->GetId(), wxEVT_LIST_ITEM_SELECTED, wxListEventHandler(VogelQuietsch::onSelectInactive));
	Connect(m_listBox_InactiveList->GetId(), wxEVT_LIST_ITEM_DESELECTED, wxListEventHandler(VogelQuietsch::onDeselectInactive));

	MyTextDropTargetActiveInactive* mdt_inActive = new MyTextDropTargetActiveInactive(m_listBox_InactiveList, m_listBox_ActiveList);
	m_listBox_InactiveList->SetDropTarget(mdt_inActive);
	Connect(m_listBox_ActiveList->GetId(), wxEVT_COMMAND_LIST_BEGIN_DRAG, wxListEventHandler(VogelQuietsch::onDragInitActive));
	Connect(m_listBox_ActiveList->GetId(), wxEVT_LIST_ITEM_SELECTED, wxListEventHandler(VogelQuietsch::onSelectActive));
	Connect(m_listBox_ActiveList->GetId(), wxEVT_LIST_ITEM_DESELECTED, wxListEventHandler(VogelQuietsch::onDeselectActive));
	
	

}



void VogelQuietsch::searchXenoCanto(wxCommandEvent &e)
{
	ApiConnector apiClient = ApiConnector();
	apiClient.checkUrlAvailability(apiClient.xenoCantoUrl);
	//StorageService().readJsonFile("D:/Users/lukas/Dokumente/VogelQuietsch/VogelQuietsch/VogelQuietsch/Services/JSON/testJSON.json");
	//wxString curlRes = curlData("https://xeno-canto.org/api/2/recordings?query=otis+tarda+q:A");
	wxArrayString birdList = apiClient.searchXenoCanto(
		m_textCtrl_SearchParameterName->GetValue(),
		m_textCtrl_SearchParameterCountry->GetValue(), 
		m_textCtrl_SearchParameterLocation->GetValue(),
		m_textCtrl_SearchParameterOther->GetValue(), 
		m_checkBox_SearchParameterIgnoreSsp->GetValue(), 
		m_checkBox_SearchParameterIgnoreSspByRegion->GetValue(), 
		m_checkBox_SearchParameterLoadAllPages->GetValue(), 
		m_checkBox_SearchParameterLimitToRegion->GetValue(), 
		m_checkBox_AddTagCountry->GetValue(),
		m_checkBox_AddTagLocation->GetValue(), 
		m_checkBox_AddTagOther->GetValue());
	//apiClient.searchXenoCanto("", "brazil", "", "", true, true, true, true, true, true, true);
	if (birdList.IsEmpty()) {
		wxLogMessage(wxT("No birds found."));
		PLOGI << "No birds found.";
	}
	else {
		HelperFunctions::fillList(birdList, m_listBox_SearchResults);
		
		
	}
	
	e.Skip();
}

//#include "VogelQuietschIcon.xpm"

VogelQuietsch::VogelQuietsch(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxFrame(parent, id, title, pos, size, style)
{
	wxInitAllImageHandlers();
	this->SetIcon(wxICON(aaaaaaVogelQuietscht)); // sets app window icon (shown in upper left corner)
	//this->SetIcon(wxIcon(VogelQuietschIcon_xpm));
	
	logger = new LoggerWrapper();
	logger->initializeLogger(plog::debug);
	PLOGI << "Starting VogelQuietsch";
	this->SetSizeHints(wxSize(1400, 800), wxDefaultSize);

	m_menubarMain = new wxMenuBar(0);
	m_menuOptions = new wxMenu();
	m_menubarMain->Append(m_menuOptions, wxT("Einstellungen"));

	this->SetMenuBar(m_menubarMain);

	wxBoxSizer* bSizer43;
	bSizer43 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer40;
	bSizer40 = new wxBoxSizer(wxHORIZONTAL);

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer48;
	bSizer48 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText_QuizzOptions = new wxStaticText(this, wxID_ANY, wxT("Quizz-Einstellungen:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_QuizzOptions->Wrap(-1);
	bSizer48->Add(m_staticText_QuizzOptions, 0, wxALL | wxEXPAND, 5);

	m_checkBox_Song = new wxCheckBox(this, wxID_ANY, wxT("Gesang"), wxDefaultPosition, wxDefaultSize, 0);
	//m_checkBox_Song->Disable();
	bSizer48->Add(m_checkBox_Song, 0, wxALL | wxEXPAND, 5);

	m_checkBox_Call = new wxCheckBox(this, wxID_ANY, wxT("Ruf"), wxDefaultPosition, wxDefaultSize, 0);
	//m_checkBox_Call->Disable();
	bSizer48->Add(m_checkBox_Call, 0, wxALL | wxEXPAND, 5);

	m_checkBox_HighQuality = new wxCheckBox(this, wxID_ANY, wxT("Hohe Qualit�t"), wxDefaultPosition, wxDefaultSize, 0);
	m_checkBox_HighQuality->SetValue(true);
	//m_checkBox_HighQuality->Disable();
	bSizer48->Add(m_checkBox_HighQuality, 0, wxALL | wxEXPAND, 5);

	m_checkBox_Sono = new wxCheckBox(this, wxID_ANY, wxT("Sono"), wxDefaultPosition, wxDefaultSize, 0);
	m_checkBox_Sono->SetValue(true);
	//m_checkBox_Sono->Disable();
	bSizer48->Add(m_checkBox_Sono, 0, wxALL | wxEXPAND, 5);

	m_checkBox_IgnoreTags = new wxCheckBox(this, wxID_ANY, wxT("Tags ignorieren"), wxDefaultPosition, wxDefaultSize, 0);
	m_checkBox_IgnoreTags->SetValue(true);
	m_checkBox_IgnoreTags->Disable();
	bSizer48->Add(m_checkBox_IgnoreTags, 0, wxALL | wxEXPAND, 5);

	m_checkBox_LimitToRegion = new wxCheckBox(this, wxID_ANY, wxT("Auf Region begrenzen"), wxDefaultPosition, wxDefaultSize, 0);
	m_checkBox_LimitToRegion->Disable();
	bSizer48->Add(m_checkBox_LimitToRegion, 0, wxALL | wxEXPAND, 5);
	
	m_button_MapQuizz = new wxButton(this, wxID_ANY, wxT("Karte"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_MapQuizz->Disable();
	bSizer48->Add(m_button_MapQuizz, 0, wxALL | wxEXPAND, 5);


	bSizer2->Add(bSizer48, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer49;
	bSizer49 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText34 = new wxStaticText(this, wxID_ANY, wxT("L�nge (s)"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText34->Wrap(-1);
	bSizer49->Add(m_staticText34, 0, wxALL | wxEXPAND, 5);

	m_textCtrl_AudioTimeLimitLower = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(30, -1), 0);
	m_textCtrl_AudioTimeLimitLower->Disable();
	bSizer49->Add(m_textCtrl_AudioTimeLimitLower, 0, wxALL | wxEXPAND, 5);

	m_staticText35 = new wxStaticText(this, wxID_ANY, wxT("-"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText35->Wrap(-1);
	bSizer49->Add(m_staticText35, 0, wxALL | wxEXPAND, 5);

	m_textCtrl_AudioTimeLimitUpper = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(30, -1), 0);
	m_textCtrl_AudioTimeLimitUpper->Disable();
	bSizer49->Add(m_textCtrl_AudioTimeLimitUpper, 0, wxALL | wxEXPAND, 5);

	m_staticText36 = new wxStaticText(this, wxID_ANY, wxT("Vogel zu inaktiv verschieben nach"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText36->Wrap(-1);
	bSizer49->Add(m_staticText36, 0, wxALL | wxEXPAND, 5);

	wxString m_choice_MoveToInactiveChoices[] = { wxT("inf."), wxT("1"), wxT("2"), wxT("3"), wxT("4"), wxT("5"), wxT("6"), wxT("7"), wxT("8"), wxT("9"), wxT("10"), wxT("11"), wxT("12"), wxT("13"), wxT("14"), wxT("15") };
	int m_choice_MoveToInactiveNChoices = sizeof(m_choice_MoveToInactiveChoices) / sizeof(wxString);
	m_choice_MoveToInactive = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxSize(40, -1), m_choice_MoveToInactiveNChoices, m_choice_MoveToInactiveChoices, 0);
	m_choice_MoveToInactive->SetSelection(0);
	m_choice_MoveToInactive->Disable();
	bSizer49->Add(m_choice_MoveToInactive, 0, wxALL | wxEXPAND, 5);

	m_staticText37 = new wxStaticText(this, wxID_ANY, wxT("x richtig beim 1. Versuch"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText37->Wrap(-1);
	bSizer49->Add(m_staticText37, 0, wxALL | wxEXPAND, 5);

	m_staticText38 = new wxStaticText(this, wxID_ANY, wxT("Max. Versuche:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText38->Wrap(-1);
	bSizer49->Add(m_staticText38, 0, wxALL | wxEXPAND, 5);

	wxString m_choice_NumberOfTriesChoices[] = { wxT("1"), wxT("2"), wxT("3"), wxT("4"), wxT("5"), wxT("6"), wxT("7"), wxT("8"), wxT("9"), wxT("10") };
	int m_choice_NumberOfTriesNChoices = sizeof(m_choice_NumberOfTriesChoices) / sizeof(wxString);
	m_choice_NumberOfTries = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxSize(40, -1), m_choice_NumberOfTriesNChoices, m_choice_NumberOfTriesChoices, 0);
	m_choice_NumberOfTries->SetSelection(1);
	bSizer49->Add(m_choice_NumberOfTries, 0, wxALL | wxEXPAND, 5);

	m_checkBox_Translate = new wxCheckBox(this, wxID_ANY, wxT("�bersetzen"), wxDefaultPosition, wxDefaultSize, 0);
	m_checkBox_Translate->Disable();
	bSizer49->Add(m_checkBox_Translate, 0, wxALL | wxEXPAND, 5);


	bSizer2->Add(bSizer49, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer47;
	bSizer47 = new wxBoxSizer(wxHORIZONTAL);

	m_button_Play = new wxButton(this, wxID_ANY, wxT("Play"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer47->Add(m_button_Play, 0, wxALL, 5);

	m_button_Pause = new wxButton(this, wxID_ANY, wxT("Pause"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_Pause->Disable();
	bSizer47->Add(m_button_Pause, 0, wxALL, 5);

	m_button_Stop = new wxButton(this, wxID_ANY, wxT("Stop"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer47->Add(m_button_Stop, 0, wxALL, 5);

	m_slider_Time = new wxSlider(this, wxID_ANY, 0, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
	bSizer47->Add(m_slider_Time, 1, wxALL | wxEXPAND, 5);

	m_staticText_Time = new wxStaticText(this, wxID_ANY, wxT("00:00 / 00:00"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_Time->Wrap(-1);
	bSizer47->Add(m_staticText_Time, 0, wxALL, 5);

	m_staticText30 = new wxStaticText(this, wxID_ANY, wxT("Lautst�rke"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText30->Wrap(-1);
	bSizer47->Add(m_staticText30, 0, wxALL, 5);

	m_slider_Volume = new wxSlider(this, wxID_ANY, 100, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
	m_slider_Volume->Disable();
	bSizer47->Add(m_slider_Volume, 0, wxALL, 5);

	m_staticText_Volume = new wxStaticText(this, wxID_ANY, wxT("100%"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_Volume->Wrap(-1);
	bSizer47->Add(m_staticText_Volume, 0, wxALL, 5);


	bSizer2->Add(bSizer47, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer98;
	bSizer98 = new wxBoxSizer(wxHORIZONTAL);
	{
		wxLogNull logNo;
		m_bitmap_Sono = new wxStaticBitmap(this, wxID_ANY, wxBitmap(), wxDefaultPosition, wxSize(750, 250), 0);
		m_bitmap_Sono->SetMinSize(wxSize(750, 250));
		wxStaticBitmap* m_bitmap_Sono_spacer = new wxStaticBitmap(this, wxID_ANY, wxBitmap(), wxDefaultPosition, wxSize(1, 250), 0);
		bSizer98->Add(m_bitmap_Sono_spacer, 0, wxALL | wxEXPAND, 5);
	}
	
	bSizer98->Add(m_bitmap_Sono, 0, wxALL | wxEXPAND, 5);


	bSizer2->Add(bSizer98, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer50;
	bSizer50 = new wxBoxSizer(wxHORIZONTAL);

	wxBoxSizer* bSizer51;
	bSizer51 = new wxBoxSizer(wxVERTICAL);

	m_staticText39 = new wxStaticText(this, wxID_ANY, wxT("Welcher Vogel ist das?"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText39->Wrap(-1);
	bSizer51->Add(m_staticText39, 0, wxALL | wxEXPAND, 5);

	m_textCtrl_BirdSelection = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
	bSizer51->Add(m_textCtrl_BirdSelection, 0, wxALL | wxEXPAND, 5);

	m_listBox_BirdSelection = new wxListCtrl(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_NO_HEADER | wxLC_SINGLE_SEL);
	bSizer51->Add(m_listBox_BirdSelection, 1, wxALL | wxEXPAND, 5);


	bSizer50->Add(bSizer51, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer52;
	bSizer52 = new wxBoxSizer(wxVERTICAL);

	m_button_StartQuizz = new wxButton(this, wxID_ANY, wxT("Quizz starten"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_StartQuizz->SetFont(wxFont(wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxEmptyString));

	bSizer52->Add(m_button_StartQuizz, 2, wxALL | wxEXPAND, 5);

	m_button_ConfirmSelection = new wxButton(this, wxID_ANY, wxT("Auswahl best�tigen"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_ConfirmSelection->SetFont(wxFont(wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxEmptyString));

	bSizer52->Add(m_button_ConfirmSelection, 2, wxALL | wxEXPAND, 5);

	m_staticText_QuizzInfo = new wxStaticText(this, wxID_ANY, wxT("hidden for now"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_QuizzInfo->Wrap(150);
	m_staticText_QuizzInfo->SetFont(m_staticText_QuizzInfo->GetFont().MakeBold());
	m_staticText_QuizzInfo->GetFont().SetPointSize(30);
	bSizer52->Add(m_staticText_QuizzInfo, 1, wxALL | wxEXPAND, 5);
	m_staticText_QuizzInfo2 = new wxStaticText(this, wxID_ANY, wxT("hidden for now"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_QuizzInfo2->Wrap(150);
	bSizer52->Add(m_staticText_QuizzInfo2, 1, wxALL | wxEXPAND, 5);

	m_button_DontKnow = new wxButton(this, wxID_ANY, wxT("Wei� nicht"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer52->Add(m_button_DontKnow, 1, wxALL | wxEXPAND, 5);

	m_button_ShowBirdOnDuckDuckGo = new wxButton(this, wxID_ANY, wxT("Vogel auf DuckDuckGo"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_ShowBirdOnDuckDuckGo->Disable();
	bSizer52->Add(m_button_ShowBirdOnDuckDuckGo, 1, wxALL | wxEXPAND, 5);

	m_button_ShowRecOnXenoCanto = new wxButton(this, wxID_ANY, wxT("Rec. auf Xeno-Canto"), wxDefaultPosition, wxDefaultSize, 0); 
	m_button_ShowRecOnXenoCanto->Disable();
	bSizer52->Add(m_button_ShowRecOnXenoCanto, 1, wxALL | wxEXPAND, 5);

	m_staticText_PlaceHolder = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_PlaceHolder->Wrap(-1);
	bSizer52->Add(m_staticText_PlaceHolder, 3, wxALL | wxEXPAND, 5);

	m_button_StopQuizz = new wxButton(this, wxID_ANY, wxT("Quizz beenden"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer52->Add(m_button_StopQuizz, 1, wxALL | wxEXPAND, 5);


	bSizer50->Add(bSizer52, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer53;
	bSizer53 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer54;
	bSizer54 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText42 = new wxStaticText(this, wxID_ANY, wxT("ID,Qual.,Count:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText42->Wrap(-1);
	m_staticText42->SetMinSize(wxSize(80, -1));
	m_staticText42->SetMaxSize(wxSize(80, -1));

	bSizer54->Add(m_staticText42, 1, wxALL | wxEXPAND, 5);

	m_staticText_BirdInfoID = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoID->Wrap(-1);
	bSizer54->Add(m_staticText_BirdInfoID, 2, wxALL | wxEXPAND, 5);


	bSizer53->Add(bSizer54, 0, wxEXPAND, 0);

	wxBoxSizer* bSizer55;
	bSizer55 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText44 = new wxStaticText(this, wxID_ANY, wxT("gen / sp / ssp:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText44->Wrap(-1);
	m_staticText44->SetMinSize(wxSize(80, -1));
	m_staticText44->SetMaxSize(wxSize(80, -1));

	bSizer55->Add(m_staticText44, 0, wxALL, 5);

	m_staticText_BirdInfoGen = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoGen->Wrap(-1);
	m_staticText_BirdInfoGen->SetFont(m_staticText_BirdInfoGen->GetFont().MakeBold());
	bSizer55->Add(m_staticText_BirdInfoGen, 0, wxALL, 5);


	bSizer53->Add(bSizer55, 0, wxEXPAND, 0);

	//wxBoxSizer* bSizer551;
	//bSizer551 = new wxBoxSizer(wxHORIZONTAL);

	//m_staticText441 = new wxStaticText(this, wxID_ANY, wxT("sp:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	//m_staticText441->Wrap(-1);
	//m_staticText441->SetMinSize(wxSize(80, -1));
	//m_staticText441->SetMaxSize(wxSize(80, -1));

	//bSizer551->Add(m_staticText441, 0, wxALL, 5);

	//m_staticText_BirdInfoSp = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	//m_staticText_BirdInfoSp->Wrap(-1);
	//bSizer551->Add(m_staticText_BirdInfoSp, 0, wxALL, 5);


	//bSizer53->Add(bSizer551, 0, wxEXPAND, 0);

	//wxBoxSizer* bSizer541;
	//bSizer541 = new wxBoxSizer(wxHORIZONTAL);

	//m_staticText421 = new wxStaticText(this, wxID_ANY, wxT("ssp:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	//m_staticText421->Wrap(-1);
	//m_staticText421->SetMinSize(wxSize(80, -1));
	//m_staticText421->SetMaxSize(wxSize(80, -1));

	//bSizer541->Add(m_staticText421, 0, wxALL, 5);

	//m_staticText_BirdInfoSsp = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	//m_staticText_BirdInfoSsp->Wrap(-1);
	//bSizer541->Add(m_staticText_BirdInfoSsp, 0, wxALL, 5);


	//bSizer53->Add(bSizer541, 0, wxEXPAND, 0);

	wxBoxSizer* bSizer5511;
	bSizer5511 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText4411 = new wxStaticText(this, wxID_ANY, wxT("Name:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText4411->Wrap(-1);
	m_staticText4411->SetMinSize(wxSize(80, -1));
	m_staticText4411->SetMaxSize(wxSize(80, -1));

	bSizer5511->Add(m_staticText4411, 0, wxALL, 5);

	m_staticText_BirdInfoName = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoName->Wrap(-1);
	m_staticText_BirdInfoName->SetFont(m_staticText_BirdInfoName->GetFont().MakeBold());
	bSizer5511->Add(m_staticText_BirdInfoName, 0, wxALL, 5);


	bSizer53->Add(bSizer5511, 1, wxEXPAND, 0);

	wxBoxSizer* bSizer542;
	bSizer542 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText422 = new wxStaticText(this, wxID_ANY, wxT("Land:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText422->Wrap(-1);
	m_staticText422->SetMinSize(wxSize(80, -1));
	m_staticText422->SetMaxSize(wxSize(80, -1));

	bSizer542->Add(m_staticText422, 0, wxALL, 5);

	m_staticText_BirdInfoCountry = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoCountry->Wrap(-1);
	bSizer542->Add(m_staticText_BirdInfoCountry, 0, wxALL, 5);


	bSizer53->Add(bSizer542, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer543;
	bSizer543 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText423 = new wxStaticText(this, wxID_ANY, wxT("Typ/sex/stage:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText423->Wrap(-1);
	m_staticText423->SetMinSize(wxSize(80, -1));
	m_staticText423->SetMaxSize(wxSize(80, -1));

	bSizer543->Add(m_staticText423, 0, wxALL, 5);

	m_staticText_BirdInfoType = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoType->Wrap(-1);
	bSizer543->Add(m_staticText_BirdInfoType, 0, wxALL, 5);


	bSizer53->Add(bSizer543, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer544;
	bSizer544 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText424 = new wxStaticText(this, wxID_ANY, wxT("Ort:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText424->Wrap(-1);
	m_staticText424->SetMinSize(wxSize(80, -1));
	m_staticText424->SetMaxSize(wxSize(80, -1));

	bSizer544->Add(m_staticText424, 0, wxALL, 5);

	m_staticText_BirdInfoLocation = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoLocation->Wrap(-1);
	bSizer544->Add(m_staticText_BirdInfoLocation,0, wxALL, 5);


	bSizer53->Add(bSizer544, 2, wxEXPAND, 5);

	wxBoxSizer* bSizer545;
	bSizer545 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText425 = new wxStaticText(this, wxID_ANY, wxT("Datum:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText425->Wrap(-1);
	m_staticText425->SetMinSize(wxSize(80, -1));
	m_staticText425->SetMaxSize(wxSize(80, -1));

	bSizer545->Add(m_staticText425, 0, wxALL, 5);

	m_staticText_BirdInfoDate = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoDate->Wrap(-1);
	bSizer545->Add(m_staticText_BirdInfoDate, 0, wxALL, 5);


	bSizer53->Add(bSizer545, 1, wxEXPAND, 5);



	wxBoxSizer* bSizer547;
	bSizer547 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText427 = new wxStaticText(this, wxID_ANY, wxT("Vogel gesehen:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText427->Wrap(-1);
	m_staticText427->SetMinSize(wxSize(80, -1));
	m_staticText427->SetMaxSize(wxSize(80, -1));

	bSizer547->Add(m_staticText427, 0, wxALL | wxEXPAND, 5);

	m_staticText_BirdInfoBirdSeen = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoBirdSeen->Wrap(-1);
	bSizer547->Add(m_staticText_BirdInfoBirdSeen, 2, wxALL | wxEXPAND, 5);


	bSizer53->Add(bSizer547, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer548;
	bSizer548 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText428 = new wxStaticText(this, wxID_ANY, wxT("Playback-ben.:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText428->Wrap(-1);
	m_staticText428->SetMinSize(wxSize(80, -1));
	m_staticText428->SetMaxSize(wxSize(80, -1));

	bSizer548->Add(m_staticText428, 1, wxALL | wxEXPAND, 5);

	m_staticText_BirdInfoUsedPlayback = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoUsedPlayback->Wrap(-1);
	bSizer548->Add(m_staticText_BirdInfoUsedPlayback, 2, wxALL | wxEXPAND, 5);


	bSizer53->Add(bSizer548, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer549;
	bSizer549 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText429 = new wxStaticText(this, wxID_ANY, wxT("Rec:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText429->Wrap(-1);
	m_staticText429->SetMinSize(wxSize(80, -1));
	m_staticText429->SetMaxSize(wxSize(80, -1));

	bSizer549->Add(m_staticText429, 1, wxALL | wxEXPAND, 5);

	m_staticText_BirdInfoRec = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoRec->Wrap(-1);
	bSizer549->Add(m_staticText_BirdInfoRec, 2, wxALL | wxEXPAND, 5);


	bSizer53->Add(bSizer549, 1, wxEXPAND, 5);

	//wxBoxSizer* bSizer5410;
	//bSizer5410 = new wxBoxSizer(wxHORIZONTAL);

	//m_staticText4210 = new wxStaticText(this, wxID_ANY, wxT("Qualit�t:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	//m_staticText4210->Wrap(-1);
	//m_staticText4210->SetMinSize(wxSize(80, -1));
	//m_staticText4210->SetMaxSize(wxSize(80, -1));

	//bSizer5410->Add(m_staticText4210, 1, wxALL | wxEXPAND, 5);

	//m_staticText_BirdInfoQuality = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	//m_staticText_BirdInfoQuality->Wrap(-1);
	//bSizer5410->Add(m_staticText_BirdInfoQuality, 2, wxALL | wxEXPAND, 5);


	//bSizer53->Add(bSizer5410, 1, wxEXPAND, 5);

	//wxBoxSizer* bSizer5411;
	//bSizer5411 = new wxBoxSizer(wxHORIZONTAL);

	//m_staticText4211 = new wxStaticText(this, wxID_ANY, wxT("numRec:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	//m_staticText4211->Wrap(-1);
	//m_staticText4211->SetMinSize(wxSize(80, -1));
	//m_staticText4211->SetMaxSize(wxSize(80, -1));

	//bSizer5411->Add(m_staticText4211, 1, wxALL | wxEXPAND, 5);

	//m_staticText_BirdInfoNumRec = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	//m_staticText_BirdInfoNumRec->Wrap(-1);
	//bSizer5411->Add(m_staticText_BirdInfoNumRec, 2, wxALL | wxEXPAND, 5);


	//bSizer53->Add(bSizer5411, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer5412;
	bSizer5412 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText4212 = new wxStaticText(this, wxID_ANY, wxT("Au�erdem:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText4212->Wrap(-1);
	m_staticText4212->SetMinSize(wxSize(80, -1));
	m_staticText4212->SetMaxSize(wxSize(80, -1));

	bSizer5412->Add(m_staticText4212, 1, wxALL | wxEXPAND, 5);

	m_staticText_BirdInfoOthers = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoOthers->Wrap(-1);
	bSizer5412->Add(m_staticText_BirdInfoOthers, 2, wxALL | wxEXPAND, 5);


	bSizer53->Add(bSizer5412, 2, wxEXPAND, 5);

	wxBoxSizer* bSizer546;
	bSizer546 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText426 = new wxStaticText(this, wxID_ANY, wxT("Info:"), wxDefaultPosition, wxSize(80, -1), wxALIGN_RIGHT);
	m_staticText426->Wrap(-1);
	m_staticText426->SetMinSize(wxSize(80, -1));
	m_staticText426->SetMaxSize(wxSize(80, -1));

	bSizer546->Add(m_staticText426, 1, wxALL | wxEXPAND, 5);

	m_staticText_BirdInfoTime = new wxStaticText(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_BirdInfoTime->Wrap(-1);
	bSizer546->Add(m_staticText_BirdInfoTime, 2, wxALL | wxEXPAND, 5);


	bSizer53->Add(bSizer546, 10, wxEXPAND, 5);

	bSizer50->Add(bSizer53, 1, wxEXPAND, 5);


	bSizer2->Add(bSizer50, 1, wxEXPAND, 5);


	bSizer40->Add(bSizer2, 3, wxEXPAND, 5);

	m_notebook = new wxNotebook(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0);
	m_notebook->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_ACTIVEBORDER));

	m_panel_ActiveInactive = new wxPanel(m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	m_panel_ActiveInactive->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_ACTIVEBORDER));

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer(wxHORIZONTAL);

	m_button_LoadList = new wxButton(m_panel_ActiveInactive, wxID_ANY, wxT("Liste Laden"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer8->Add(m_button_LoadList, 1, wxALL | wxEXPAND, 5);

	m_button_LoadProfile = new wxButton(m_panel_ActiveInactive, wxID_ANY, wxT("Profil laden"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_LoadProfile->Disable();
	bSizer8->Add(m_button_LoadProfile, 1, wxALL | wxEXPAND, 5);

	m_button_CreateProfile = new wxButton(m_panel_ActiveInactive, wxID_ANY, wxT("Profil erstellen"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_CreateProfile->Disable();
	bSizer8->Add(m_button_CreateProfile, 1, wxALL | wxEXPAND, 5);

	m_button_SortLists = new wxButton(m_panel_ActiveInactive, wxID_ANY, wxT("Listen Sortieren"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_SortLists->Disable();
	bSizer8->Add(m_button_SortLists, 1, wxALL | wxEXPAND, 5);


	bSizer3->Add(bSizer8, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer(wxHORIZONTAL);

	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer(wxVERTICAL);

	m_staticText5 = new wxStaticText(m_panel_ActiveInactive, wxID_ANY, wxT("Profil:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText5->Wrap(-1);
	bSizer10->Add(m_staticText5, 0, wxALIGN_RIGHT | wxALL, 5);

	m_staticText6 = new wxStaticText(m_panel_ActiveInactive, wxID_ANY, wxT("Geladene Liste:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText6->Wrap(-1);
	bSizer10->Add(m_staticText6, 0, wxALIGN_RIGHT | wxALL, 5);


	bSizer9->Add(bSizer10, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer(wxVERTICAL);

	m_staticText_LoadedProfile = new wxStaticText(m_panel_ActiveInactive, wxID_ANY, wxT("Kein Profil geladen"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_LoadedProfile->Wrap(-1);
	bSizer12->Add(m_staticText_LoadedProfile, 0, wxALL, 5);

	m_staticText_LoadedList = new wxStaticText(m_panel_ActiveInactive, wxID_ANY, wxT("Keine Liste geladen"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_LoadedList->Wrap(-1);
	bSizer12->Add(m_staticText_LoadedList, 0, wxALL, 5);


	bSizer9->Add(bSizer12, 1, wxEXPAND, 5);


	bSizer3->Add(bSizer9, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText3 = new wxStaticText(m_panel_ActiveInactive, wxID_ANY, wxT("Aktive Liste"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText3->Wrap(-1);
	bSizer5->Add(m_staticText3, 1, wxALL | wxEXPAND, 5);

	m_staticText4 = new wxStaticText(m_panel_ActiveInactive, wxID_ANY, wxT("Inaktive Liste"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText4->Wrap(-1);
	bSizer5->Add(m_staticText4, 1, wxALL | wxEXPAND, 5);


	bSizer4->Add(bSizer5, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer(wxHORIZONTAL);

	m_listBox_ActiveList = new wxListCtrl(m_panel_ActiveInactive, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_NO_HEADER);
	bSizer6->Add(m_listBox_ActiveList, 1, wxALL | wxEXPAND, 1);

	m_listBox_InactiveList = new wxListCtrl(m_panel_ActiveInactive, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_NO_HEADER);
	bSizer6->Add(m_listBox_InactiveList, 1, wxALL | wxEXPAND, 1);


	bSizer4->Add(bSizer6, 1, wxEXPAND, 5);


	bSizer3->Add(bSizer4, 1, wxEXPAND, 5);


	m_panel_ActiveInactive->SetSizer(bSizer3);
	m_panel_ActiveInactive->Layout();
	bSizer3->Fit(m_panel_ActiveInactive);
	m_notebook->AddPage(m_panel_ActiveInactive, wxT("Aktiv/Inaktiv"), true);
	m_panel_ListCreateEdit = new wxPanel(m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	m_panel_ListCreateEdit->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_ACTIVEBORDER));

	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer(wxHORIZONTAL);

	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer(wxVERTICAL);

	m_staticText21 = new wxStaticText(m_panel_ListCreateEdit, wxID_ANY, wxT("Name (eng/la):"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText21->Wrap(-1);
	bSizer21->Add(m_staticText21, 1, wxALIGN_RIGHT | wxALL, 5);

	m_staticText22 = new wxStaticText(m_panel_ListCreateEdit, wxID_ANY, wxT("Land:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText22->Wrap(-1);
	bSizer21->Add(m_staticText22, 1, wxALIGN_RIGHT | wxALL, 5);

	m_staticText23 = new wxStaticText(m_panel_ListCreateEdit, wxID_ANY, wxT("Ort:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText23->Wrap(-1);
	bSizer21->Add(m_staticText23, 1, wxALIGN_RIGHT | wxALL, 5);

	m_staticText24 = new wxStaticText(m_panel_ListCreateEdit, wxID_ANY, wxT("Sonstiges:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText24->Wrap(-1);
	bSizer21->Add(m_staticText24, 1, wxALIGN_RIGHT | wxALL, 5);


	bSizer20->Add(bSizer21, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer(wxVERTICAL);

	m_textCtrl_SearchParameterName = new wxTextCtrl(m_panel_ListCreateEdit, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
	m_textCtrl_SearchParameterName->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));

	bSizer24->Add(m_textCtrl_SearchParameterName, 1, wxALL | wxEXPAND, 5);

	m_textCtrl_SearchParameterCountry = new wxTextCtrl(m_panel_ListCreateEdit, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
	bSizer24->Add(m_textCtrl_SearchParameterCountry, 1, wxALL | wxEXPAND, 5);

	m_textCtrl_SearchParameterLocation = new wxTextCtrl(m_panel_ListCreateEdit, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
	bSizer24->Add(m_textCtrl_SearchParameterLocation, 1, wxALL | wxEXPAND, 5);

	m_textCtrl_SearchParameterOther = new wxTextCtrl(m_panel_ListCreateEdit, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
	bSizer24->Add(m_textCtrl_SearchParameterOther, 1, wxALL | wxEXPAND, 5);


	bSizer20->Add(bSizer24, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer26;
	bSizer26 = new wxBoxSizer(wxVERTICAL);

	m_staticText25 = new wxStaticText(m_panel_ListCreateEdit, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0);
	m_staticText25->Wrap(-1);
	bSizer26->Add(m_staticText25, 1, wxALL, 5);

	m_checkBox_AddTagCountry = new wxCheckBox(m_panel_ListCreateEdit, wxID_ANY, wxT("tag hinzuf�gen"), wxDefaultPosition, wxDefaultSize, 0);
	m_checkBox_AddTagCountry->Disable();
	bSizer26->Add(m_checkBox_AddTagCountry, 1, wxALL, 5);

	m_checkBox_AddTagLocation = new wxCheckBox(m_panel_ListCreateEdit, wxID_ANY, wxT("tag hinzuf�gen"), wxDefaultPosition, wxDefaultSize, 0);
	m_checkBox_AddTagLocation->Disable();
	bSizer26->Add(m_checkBox_AddTagLocation, 1, wxALL, 5);

	m_checkBox_AddTagOther = new wxCheckBox(m_panel_ListCreateEdit, wxID_ANY, wxT("tag hinzuf�gen"), wxDefaultPosition, wxDefaultSize, 0);
	m_checkBox_AddTagOther->Disable();
	bSizer26->Add(m_checkBox_AddTagOther, 1, wxALL, 5);


	bSizer20->Add(bSizer26, 0, wxEXPAND, 5);


	bSizer13->Add(bSizer20, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer27;
	bSizer27 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer28;
	bSizer28 = new wxBoxSizer(wxHORIZONTAL);

	m_checkBox_SearchParameterIgnoreSsp = new wxCheckBox(m_panel_ListCreateEdit, wxID_ANY, wxT("ssp ignorieren"), wxDefaultPosition, wxDefaultSize, 0);
	m_checkBox_SearchParameterIgnoreSsp->SetValue(true);
	m_checkBox_SearchParameterIgnoreSsp->Disable();
	bSizer28->Add(m_checkBox_SearchParameterIgnoreSsp, 0, wxALL | wxEXPAND, 5);

	m_checkBox_SearchParameterIgnoreSspByRegion = new wxCheckBox(m_panel_ListCreateEdit, wxID_ANY, wxT("ssp by region ignorieren"), wxDefaultPosition, wxDefaultSize, 0);
	m_checkBox_SearchParameterIgnoreSspByRegion->SetValue(true);
	m_checkBox_SearchParameterIgnoreSspByRegion->Disable();
	bSizer28->Add(m_checkBox_SearchParameterIgnoreSspByRegion, 0, wxALL | wxEXPAND, 5);

	m_checkBox_SearchParameterLoadAllPages = new wxCheckBox(m_panel_ListCreateEdit, wxID_ANY, wxT("Mehr als eine Seite laden"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer28->Add(m_checkBox_SearchParameterLoadAllPages, 0, wxALL | wxEXPAND, 5);

	m_checkBox_SearchParameterLimitToRegion = new wxCheckBox(m_panel_ListCreateEdit, wxID_ANY, wxT("Region eingrenzen"), wxDefaultPosition, wxDefaultSize, 0);
	m_checkBox_SearchParameterLimitToRegion->Disable();
	bSizer28->Add(m_checkBox_SearchParameterLimitToRegion, 0, wxALL | wxEXPAND, 5);

	m_button_SearchParameterMap = new wxButton(m_panel_ListCreateEdit, wxID_ANY, wxT("Karte\nanzeigen"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_SearchParameterMap->Disable();
	bSizer28->Add(m_button_SearchParameterMap, 0, wxALL | wxEXPAND, 1);


	bSizer27->Add(bSizer28, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer32;
	bSizer32 = new wxBoxSizer(wxHORIZONTAL);

	m_button_SearchXenoCantoDb = new wxButton(m_panel_ListCreateEdit, wxID_ANY, wxT("Xeno Canto durchsuchen"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer32->Add(m_button_SearchXenoCantoDb, 1, wxALL | wxEXPAND, 5);

	m_staticText28 = new wxStaticText(m_panel_ListCreateEdit, wxID_ANY, wxT("Vogelliste:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText28->Wrap(-1);
	bSizer32->Add(m_staticText28, 1, wxALL | wxEXPAND, 5);


	bSizer27->Add(bSizer32, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer31;
	bSizer31 = new wxBoxSizer(wxHORIZONTAL);

	m_staticText_SearchResults = new wxStaticText(m_panel_ListCreateEdit, wxID_ANY, wxT("Suchergebnisse"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_SearchResults->Wrap(-1);
	bSizer31->Add(m_staticText_SearchResults, 1, wxALL | wxEXPAND, 5);

	m_staticText_LoadedListEdit = new wxStaticText(m_panel_ListCreateEdit, wxID_ANY, wxT("Vogelliste"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText_LoadedListEdit->Wrap(-1);
	bSizer31->Add(m_staticText_LoadedListEdit, 1, wxALL | wxEXPAND, 5);


	bSizer27->Add(bSizer31, 0, wxEXPAND, 5);

	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer(wxHORIZONTAL);

	m_listBox_SearchResults = new wxListCtrl(m_panel_ListCreateEdit, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_NO_HEADER);
	bSizer29->Add(m_listBox_SearchResults, 1, wxALL | wxEXPAND, 5);

	m_listBox_LoadedList = new wxListCtrl(m_panel_ListCreateEdit, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_NO_HEADER);
	bSizer29->Add(m_listBox_LoadedList, 1, wxALL | wxEXPAND, 5);

	bSizer27->Add(bSizer29, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer30;
	bSizer30 = new wxBoxSizer(wxHORIZONTAL);

	m_button_LoadListEdit = new wxButton(m_panel_ListCreateEdit, wxID_ANY, wxT("Liste Laden"), wxDefaultPosition, wxDefaultSize, 0);
	//m_button_LoadListEdit->Disable();
	bSizer30->Add(m_button_LoadListEdit, 1, wxALL | wxEXPAND, 5);

	m_button_SortListEdit = new wxButton(m_panel_ListCreateEdit, wxID_ANY, wxT("Listen sortieren"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_SortListEdit->Disable();
	bSizer30->Add(m_button_SortListEdit, 1, wxALL | wxEXPAND, 5);

	m_button_RemoveCollectionEdit = new wxButton(m_panel_ListCreateEdit, wxID_ANY, wxT("Auswahl entfernen"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_RemoveCollectionEdit->Disable();
	bSizer30->Add(m_button_RemoveCollectionEdit, 1, wxALL | wxEXPAND, 5);

	m_button_SaveListEdit = new wxButton(m_panel_ListCreateEdit, wxID_ANY, wxT("Liste speichern"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer30->Add(m_button_SaveListEdit, 1, wxALL | wxEXPAND, 5);


	bSizer27->Add(bSizer30, 0, wxEXPAND, 5);


	bSizer13->Add(bSizer27, 1, wxEXPAND, 5);


	m_panel_ListCreateEdit->SetSizer(bSizer13);
	m_panel_ListCreateEdit->Layout();
	//m_panel_ListCreateEdit->Disable();
	bSizer13->Fit(m_panel_ListCreateEdit);
	m_notebook->AddPage(m_panel_ListCreateEdit, wxT("Listen erstellen/bearbeiten"), false);
	m_panel_Statistics = new wxPanel(m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxBoxSizer* bSizer100;
	bSizer100 = new wxBoxSizer(wxVERTICAL);

	m_dataViewListCtrl2 = new wxDataViewListCtrl(m_panel_Statistics, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0);
	bSizer100->Add(m_dataViewListCtrl2, 1, wxALL | wxEXPAND, 5);


	m_panel_Statistics->SetSizer(bSizer100);
	m_panel_Statistics->Layout();
	bSizer100->Fit(m_panel_Statistics);
	m_notebook->AddPage(m_panel_Statistics, wxT("Statistik"), false);
	m_panel_History = new wxPanel(m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxBoxSizer* bSizer99;
	bSizer99 = new wxBoxSizer(wxVERTICAL);

	m_dataViewListCtrl1 = new wxDataViewListCtrl(m_panel_History, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0);
	bSizer99->Add(m_dataViewListCtrl1, 1, wxALL | wxEXPAND, 5);


	m_panel_History->SetSizer(bSizer99);
	m_panel_History->Layout();
	bSizer99->Fit(m_panel_History);
	m_notebook->AddPage(m_panel_History, wxT("Verlauf"), false);
	m_panel_Dictionary = new wxPanel(m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	m_notebook->AddPage(m_panel_Dictionary, wxT("W�rterbuch"), false);

	bSizer40->Add(m_notebook, 2, wxEXPAND, 0);


	bSizer43->Add(bSizer40, 1, wxEXPAND, 5);


	this->SetSizer(bSizer43);
	this->Layout();

	this->Centre(wxBOTH);


	bindGuiElements();
	HelperFunctions::initListCtrl(m_listBox_LoadedList);
	HelperFunctions::initListCtrl(m_listBox_SearchResults);
	HelperFunctions::initListCtrl(m_listBox_ActiveList);
	HelperFunctions::initListCtrl(m_listBox_InactiveList);
	HelperFunctions::initListCtrl(m_listBox_BirdSelection);
	mediaCtrlWrapper = new MediaCtrlWrapper(this,m_slider_Time, m_staticText_Time, m_button_Play, m_button_Pause, m_button_Stop);
	//PLOGI << "try to start mediaplayer!";
	//wxString string("https://ia902203.us.archive.org/11/items/testmp3testfile/mpthreetest.mp3");
	//wxString string1("./Media/warning.mp3");
	mediaCtrlWrapper->PlayWarning();
	vogelQuietschGame = new VogelQuietschGame(m_button_StartQuizz, m_button_ConfirmSelection, m_button_DontKnow, m_button_StopQuizz, m_button_ShowRecOnXenoCanto, m_button_ShowBirdOnDuckDuckGo,
		m_listBox_ActiveList, m_listBox_BirdSelection,
		m_staticText_BirdInfoID, m_staticText_BirdInfoGen,m_staticText_BirdInfoSp, m_staticText_BirdInfoSsp,m_staticText_BirdInfoName,
		m_staticText_BirdInfoCountry, m_staticText_BirdInfoType, m_staticText_BirdInfoLocation, m_staticText_BirdInfoDate, 
		m_staticText_BirdInfoTime, m_staticText_BirdInfoBirdSeen, m_staticText_BirdInfoUsedPlayback, m_staticText_BirdInfoRec, 
		m_staticText_BirdInfoQuality, m_staticText_BirdInfoNumRec, m_staticText_BirdInfoOthers, m_staticText_QuizzInfo, m_staticText_QuizzInfo2,
		m_checkBox_Song, m_checkBox_Call, m_checkBox_HighQuality, m_checkBox_Sono, m_choice_NumberOfTries,
		m_bitmap_Sono, m_textCtrl_BirdSelection,
		mediaCtrlWrapper);


	//SQLiteWrapper::insertData("test2.db");
}


void VogelQuietsch::onDragInitSearchToEdit(wxListEvent& event)
{
	wxTextDataObject tdo(ConverterHelper().concatArrayString(searchResultsSelection, "|||"));
	wxDropSource tds(tdo, m_listBox_SearchResults);
	tds.DoDragDrop(wxDrag_CopyOnly);
}
void VogelQuietsch::onSelectSearchToEdit(wxListEvent& event)
{
	wxString text = m_listBox_SearchResults->GetItemText(event.GetIndex());
	if (searchResultsSelection.Index(text) == wxNOT_FOUND ) {
		searchResultsSelection.Add(text);
	}
}
void VogelQuietsch::onDeselectSearchToEdit(wxListEvent& event)
{
	wxString text = m_listBox_SearchResults->GetItemText(event.GetIndex());
	searchResultsSelection.Remove(text);
}

void VogelQuietsch::onDragInitInactive(wxListEvent& event)
{
	wxTextDataObject tdo(ConverterHelper().concatArrayString(inactiveSelection, "|||"));
	wxDropSource tds(tdo, m_listBox_InactiveList);
	tds.DoDragDrop(wxDragMove);
}
void VogelQuietsch::onSelectInactive(wxListEvent& event)
{
	wxString text = m_listBox_InactiveList->GetItemText(event.GetIndex());
	if (inactiveSelection.Index(text) == wxNOT_FOUND) {
		inactiveSelection.Add(text);
	}
}
void VogelQuietsch::onDeselectInactive(wxListEvent& event)
{
	wxString text = m_listBox_InactiveList->GetItemText(event.GetIndex());
	inactiveSelection.Remove(text);
}

void VogelQuietsch::onDragInitActive(wxListEvent& event)
{
	wxTextDataObject tdo(ConverterHelper().concatArrayString(activeSelection, "|||"));
	wxDropSource tds(tdo, m_listBox_ActiveList);
	tds.DoDragDrop(wxDragMove);
}
void VogelQuietsch::onSelectActive(wxListEvent& event)
{
	wxString text = m_listBox_ActiveList->GetItemText(event.GetIndex());
	if (activeSelection.Index(text) == wxNOT_FOUND) {
		activeSelection.Add(text);
	}
}
void VogelQuietsch::onDeselectActive(wxListEvent& event)
{
	wxString text = m_listBox_ActiveList->GetItemText(event.GetIndex());
	activeSelection.Remove(text);
}
void VogelQuietsch::onLoadBirdListToInactive(wxCommandEvent& event)
{
	wxFileDialog
		openFileDialog(this, _("Open BirdList file"), "", "",
			"txt files (*.txt)|*.txt", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openFileDialog.ShowModal() == wxID_CANCEL)
		return;     // the user changed idea...

	// proceed loading the file chosen by the user;
	// this can be done with e.g. wxWidgets input streams:
	StorageService storageService;
	wxArrayString birdList = storageService.readTxtFile(openFileDialog.GetPath());
	m_listBox_InactiveList->ClearAll();
	m_listBox_ActiveList->ClearAll();
	HelperFunctions::initListCtrl(m_listBox_InactiveList);
	HelperFunctions::initListCtrl(m_listBox_ActiveList);
	HelperFunctions::fillList(birdList, m_listBox_InactiveList);
	m_staticText_LoadedList->SetLabel(openFileDialog.GetPath());
}
void VogelQuietsch::onLoadBirdListToEdit(wxCommandEvent& event)
{
	wxFileDialog
		openFileDialog(this, _("Open BirdList file"), "", "",
			"txt files (*.txt)|*.txt", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openFileDialog.ShowModal() == wxID_CANCEL)
		return;     // the user changed idea...

	// proceed loading the file chosen by the user;
	// this can be done with e.g. wxWidgets input streams:
	StorageService storageService;
	wxArrayString birdList = storageService.readTxtFile(openFileDialog.GetPath());
	
	m_listBox_LoadedList->ClearAll();
	HelperFunctions::initListCtrl(m_listBox_LoadedList);
	HelperFunctions::fillList(birdList, m_listBox_LoadedList);
	m_staticText_LoadedListEdit->SetLabel(openFileDialog.GetPath());
}

void VogelQuietsch::onSaveBirdListFromEdit(wxCommandEvent& evt)
{
	wxFileDialog
		openFileDialog(this, _("Save BirdList file"), "", "",
			"txt files (*.txt)|*.txt", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (openFileDialog.ShowModal() == wxID_CANCEL)
		return;     // the user changed idea...

	// proceed loading the file chosen by the user;
	// this can be done with e.g. wxWidgets input streams:

	wxArrayString BirdList;

	for (int i = 0; i < m_listBox_LoadedList->GetItemCount();i++) {
		BirdList.Add(m_listBox_LoadedList->GetItemText(i));
	}
	PLOGD << BirdList.size();
	StorageService storageService;
	storageService.writeTxtFile(openFileDialog.GetPath(), BirdList);

	m_staticText_LoadedListEdit->SetLabel(openFileDialog.GetPath());
}






VogelQuietsch::~VogelQuietsch()
{

	delete mediaCtrlWrapper;
	delete logger;
	delete vogelQuietschGame;
}
MyTextDropTarget::MyTextDropTarget(wxListCtrl* owner)
{
	m_owner = owner;
}

bool MyTextDropTarget::OnDropText(wxCoord x, wxCoord y,
	const wxString& data)
{

	wxArrayString dataArray = ConverterHelper().splitArrayString(data, "|||");

	for (wxString string : dataArray) {
		if (m_owner->FindItem(-1, string, false) == -1) {
			m_owner->InsertItem(0, string);
			PLOGI << string;
		}
	}


	return true;
}
MyTextDropTargetActiveInactive::MyTextDropTargetActiveInactive(wxListCtrl* target, wxListCtrl* source)
{
	m_target = target;
	m_source = source;
}

bool MyTextDropTargetActiveInactive::OnDropText(wxCoord x, wxCoord y,
	const wxString & data)
{

	wxArrayString dataArray = ConverterHelper().splitArrayString(data, "|||");

	for (wxString string : dataArray) {
		if (m_target->FindItem(-1, string, false) == -1) {
			m_target->InsertItem(0, string);
			long index = m_source->FindItem(-1, string, false);
			if (index >= 0) {
				m_source->DeleteItem(index);
			}

			PLOGI << string;
		}	
	}
	return true;
}
