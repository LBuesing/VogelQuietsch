
#include "HelperFunctions.h"
#include "LoggerWrapper.h"
HelperFunctions::HelperFunctions()
{
}
void HelperFunctions::fillList(wxArrayString arrayString, wxListCtrl* listCtrl) {
	initListCtrl(listCtrl);
	for (wxString string : arrayString) {
		if (string != "") {
			listCtrl->InsertItem(0, string);
		}
	}
}

void HelperFunctions::initListCtrl(wxListCtrl* target)
{
	target->ClearAll();
	target->InsertColumn(0, _("Birdlist"), wxLIST_FORMAT_LEFT, -1);
	target->SetColumnWidth(0, 500);
}
void HelperFunctions::SyncLists(wxListCtrl* fromList, wxListCtrl* toList)
{
	toList->ClearAll();
	initListCtrl(toList);
	for (int i = 0; i < fromList->GetItemCount(); i++) {
		toList->InsertItem(0, fromList->GetItemText(i));
	}
}
wxArrayString HelperFunctions::GetListEntries(wxListCtrl* fromList) {
	wxArrayString entries;
	for (int i = 0; i < fromList->GetItemCount(); i++) {
		entries.Add(fromList->GetItemText(i));
	}
	return entries;
}

void HelperFunctions::regexBirdList(wxListCtrl* birdListCtrl, wxArrayString birdList, wxString search) {
	std::string searchString = search.ToStdString();

	transform(searchString.begin(), searchString.end(), searchString.begin(), ::tolower);
	std::regex regex ("[A-Za-z0-9 ()'\\\\\"]*("+ searchString+")[A-Za-z0-9 ()'\\\\\"]*");
	birdListCtrl->ClearAll();
	initListCtrl(birdListCtrl);
	for (wxString bird : birdList) {
		std::string entryToCheck = bird.ToStdString();
		transform(entryToCheck.begin(), entryToCheck.end(), entryToCheck.begin(), ::tolower);
		if (regex_match(entryToCheck, regex)) {
			birdListCtrl->InsertItem(0, bird);
		}
	}
}