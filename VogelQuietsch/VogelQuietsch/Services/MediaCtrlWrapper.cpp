#include "MediaCtrlWrapper.h"
#include "LoggerWrapper.h"

MediaCtrlWrapper::MediaCtrlWrapper(wxWindow* parent, wxSlider* TimeSlider, wxStaticText* m_staticText_Time, wxButton* m_button_Play, wxButton* m_button_Pause, wxButton* m_button_Stop) {
    PLOGI << "Initializing MediaCtrlWrapper";
    m_bdoNotify = new boolean(true);
    mediaCtrl = new wxMediaCtrl();
	mediaCtrl->Create(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxMEDIABACKEND_WMP10, wxDefaultValidator, "mediaCtrl"); //wxMEDIABACKEND_WMP10
	mediaCtrl->Bind(wxEVT_MEDIA_LOADED, &MediaCtrlWrapper::OnMediaLoaded, this);
    timer = new wxMediaPlayerTimer(mediaCtrl, TimeSlider, m_bdoNotify, m_staticText_Time);
    timer->Start(100);
    this->PlayButton = m_button_Play;
    this->PauseButton = m_button_Pause;
    this->StopButton = m_button_Stop;
    this->PlayButton->Bind(wxEVT_BUTTON, &MediaCtrlWrapper::onPlay, this);
    this->PauseButton->Bind(wxEVT_BUTTON, &MediaCtrlWrapper::onPause, this);
    this->StopButton->Bind(wxEVT_BUTTON, &MediaCtrlWrapper::onStop, this);
}
void MediaCtrlWrapper::PlayWarning() {
    PlayMedia(wxT("./Media/warning.mp3"), false);
}

void MediaCtrlWrapper::Stop()
{
    
    mediaCtrl->Stop();
  
}
void MediaCtrlWrapper::Pause()
{
   
    if (!mediaCtrl->Pause()) {
        wxMessageBox("Could not pause!");
        PLOGE << "Could not pause!";
    }
    else {
        PLOGI << "Paused!";
    }
  
}
void MediaCtrlWrapper::Play()
{
    
    if (!mediaCtrl->Play()) {
        wxMessageBox("Could not play media!");
        PLOGE << "Could not play media!";
    }
    else {
        PLOGI << "Started playing media!";
    }
 }
void MediaCtrlWrapper::SetVolume(double volume)
{
    mediaCtrl->SetVolume(volume);
}
double MediaCtrlWrapper::GetVolume()
{
    return mediaCtrl->GetVolume();
}
void MediaCtrlWrapper::SetPosition(double position)
{
}
double MediaCtrlWrapper::GetPosition()
{
    return 0.0;
}
MediaCtrlWrapper::~MediaCtrlWrapper()
{
    delete timer;
    delete m_bdoNotify;
    
}
void MediaCtrlWrapper::PlayMedia(const wxString& path, boolean useControls) {
    setDoNotify(useControls);
    wxURI uripath(path);
    if (uripath.IsReference())
    {
        if (!mediaCtrl->Load(path))
        {
            wxMessageBox("Couldn't load file!");
        }
    }
    else
    {
        if (!mediaCtrl->Load(uripath))
        {
            wxMessageBox("Couldn't load URL!");
        }
    }
}

void MediaCtrlWrapper::OnMediaLoaded(wxMediaEvent& event) {

    Play();

}

void MediaCtrlWrapper::setDoNotify(boolean doNotify)
{
    *m_bdoNotify = doNotify;
}

void MediaCtrlWrapper::onPlay(wxCommandEvent& event)
{
    Play();
}

void MediaCtrlWrapper::onPause(wxCommandEvent& event)
{
    
    Pause();

}

void MediaCtrlWrapper::onStop(wxCommandEvent& event)
{
    Stop();
}

wxMediaPlayerTimer::wxMediaPlayerTimer(wxMediaCtrl* mediaCtrl, wxSlider* TimeSlider, boolean* m_bdoNotify, wxStaticText* m_staticText_Time)
{
    PLOGI << "Initialize wxMediaPlayerTimer";
    this->mediaCtrl = mediaCtrl;
    this->TimeSlider = TimeSlider;
    m_bIsBeingDragged = false;
    this->TimeSlider->Bind(wxEVT_SCROLL_THUMBTRACK, &wxMediaPlayerTimer::OnBeginSeek, this);
    this->TimeSlider->Bind(wxEVT_SCROLL_THUMBRELEASE, &wxMediaPlayerTimer::OnEndSeek, this);
    this->m_bdoNotify = m_bdoNotify;
    this->TimeText = m_staticText_Time;

}

void wxMediaPlayerTimer::Notify()
{
   

//    wxMediaPlayerNotebookPage* currentpage =
//        (wxMediaPlayerNotebookPage*)m_frame->m_notebook->GetCurrentPage();
//    wxMediaCtrl* currentMediaCtrl = currentpage->m_mediactrl;
//
    // Number of minutes/seconds total
    wxLongLong llLength = mediaCtrl->Length();
    int nMinutes = (int)(llLength / 60000).GetValue();
    int nSeconds = (int)((llLength % 60000) / 1000).GetValue();

    // Duration string (i.e. MM:SS)
    wxString sDuration;
    sDuration.Printf("%2i:%02i", nMinutes, nSeconds);


    // Number of minutes/seconds total
    wxLongLong llTell = mediaCtrl->Tell();
    nMinutes = (int)(llTell / 60000).GetValue();
    nSeconds = (int)((llTell % 60000) / 1000).GetValue();

    // Position string (i.e. MM:SS)
    wxString sPosition;
    sPosition.Printf("%2i:%02i", nMinutes, nSeconds);
    if (!doNotify()) {
        return;
    }

    TimeText->SetLabelText(sPosition + " / " + sDuration);
//
//
//    // Set the third item in the listctrl entry to the duration string
//    if (currentpage->m_nLastFileId >= 0)
//        currentpage->m_playlist->SetItem(
//            currentpage->m_nLastFileId, 2, sDuration);
//
//    // Setup the slider and gauge min/max values
    TimeSlider->SetRange(0, (int)(llLength / 1000).GetValue());
//    currentpage->m_gauge->SetRange(100);
//
//
//    // if the slider is not being dragged then update it with the song position
    if (IsBeingDragged() == false)
        TimeSlider->SetValue((long)(llTell / 1000).GetValue());
//
//
//    // Update the gauge with the download progress
//    wxLongLong llDownloadProgress =
//        currentpage->m_mediactrl->GetDownloadProgress();
//    wxLongLong llDownloadTotal =
//        currentpage->m_mediactrl->GetDownloadTotal();
//
//    if (llDownloadTotal.GetValue() != 0)
//    {
//        currentpage->m_gauge->SetValue(
//            (int)((llDownloadProgress * 100) / llDownloadTotal).GetValue()
//        );
//    }
//
//    // GetBestSize holds the original video size
//    wxSize videoSize = currentMediaCtrl->GetBestSize();
//
//    // Now the big part - set the status bar text to
//    // hold various metadata about the media
//#if wxUSE_STATUSBAR
//    m_frame->SetStatusText(wxString::Format(
//        "Size(x,y):%i,%i "
//        "Position:%s/%s Speed:%1.1fx "
//        "State:%s Loops:%i D/T:[%i]/[%i] V:%i%%",
//        videoSize.x,
//        videoSize.y,
//        sPosition,
//        sDuration,
//        currentMediaCtrl->GetPlaybackRate(),
//        wxGetMediaStateText(currentpage->m_mediactrl->GetState()),
//        currentpage->m_nLoops,
//        (int)llDownloadProgress.GetValue(),
//        (int)llDownloadTotal.GetValue(),
//        (int)(currentpage->m_mediactrl->GetVolume() * 100)));
//#endif // wxUSE_STATUSBAR
}
void wxMediaPlayerTimer::OnBeginSeek(wxScrollEvent& WXUNUSED(event))
{
    m_bIsBeingDragged = true;
}

void wxMediaPlayerTimer::OnEndSeek(wxScrollEvent& WXUNUSED)
{
    if (mediaCtrl->Seek(
        TimeSlider->GetValue() * 1000
    ) == wxInvalidOffset)
        wxMessageBox("Couldn't seek in movie!");

    m_bIsBeingDragged = false;
}

boolean wxMediaPlayerTimer::IsBeingDragged()
{
    return m_bIsBeingDragged;
}

boolean wxMediaPlayerTimer::doNotify()
{
    return *m_bdoNotify;
}
