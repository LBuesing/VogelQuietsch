#pragma once
#include <wx/wx.h>
#include <JSON/jsonreader.h>
#include <json/json.h>
#include <wx/tokenzr.h>

class ConverterHelper
{
public:
	ConverterHelper();
	wxString concatArrayString(const wxArrayString& array, wxString sep);
	wxArrayString splitArrayString(const wxString& string, wxString sep);
	wxJSONValue parseWxJSON(wxString inputString);
	Json::Value parseJSON(std::string inputString);
	wxString intToWxString(int integer);
	int stringToInt(std::string inputString);
	std::string wxStringToString(wxString inputString);
	wxString createBirdName(std::string en, std::string gen, std::string sp);
	wxString getEnFromBirdName(wxString birdName);
	wxString getGenFromBirdName(wxString birdName);
	wxString getSpFromBirdName(wxString birdName);
	wxString getGenAndSpFromBirdName(wxString birdName);

};
