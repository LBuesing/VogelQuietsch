
#include "ConverterHelper.h"
#include "LoggerWrapper.h"
ConverterHelper::ConverterHelper()
{

}

wxString ConverterHelper::concatArrayString(const wxArrayString& array, wxString sep)
{
	PLOGI << "Concatenating wxArrayString into wxString";
    wxString result;
    for (size_t i = 0; i < array.GetCount()-1; i++)
    {
        result.Append(array.Item(i)).Append(sep);
    }
	result.Append(array.Item(array.GetCount() - 1));
    return result;
}

wxArrayString ConverterHelper::splitArrayString(const wxString& string, wxString sep)
{
	PLOGI << "Splitting wxString into wxArrayString";
	wxStringTokenizer tokenizer(string, sep);
	wxArrayString result;
	while (tokenizer.HasMoreTokens())
	{
		wxString token = tokenizer.GetNextToken();
		// process token here
		
		if (token != "") {
			PLOGI << "Adding token to arraylist: " << token;
			result.Add(token);
		}
		
	}


	return result;
}

wxJSONValue ConverterHelper::parseWxJSON(wxString inputString)
{
	PLOGI << "Parsing wxString into wxJSONValue";
	// construct the JSON root object
	wxJSONValue  root = wxJSONValue();

	// construct a JSON parser
	wxJSONReader reader = wxJSONReader();

	// now read the JSON text and store it in the 'root' structure
	// check for errors before retreiving values...
	int numErrors = reader.Parse(inputString, &root);
	if (numErrors > 0) {
		PLOGE << "ERROR: the JSON document is not well-formed";
		wxString errorMessage = "ERROR: the JSON document is not well-formed";
		const wxArrayString& errors = reader.GetErrors();
		// now print the errors array
		wxLogMessage(errorMessage);

		return root;
	}
	//wxLogMessage("Members of testJSON"+ConverterHelper().concatArrayString(root.GetMemberNames(), ","));
	//wxLogMessage(root["recordings"].ItemAt(0)["file"].AsString());
	return root;
}


Json::Value ConverterHelper::parseJSON(std::string inputString)
{
	PLOGI << "Parsing std::string into Json::Value";
	Json::Value jsonData;
	Json::Reader jsonReader;
	
	if (!jsonReader.parse(inputString, jsonData)) {
		PLOGE << "ERROR: the JSON document is not well-formed";
	}
	return jsonData;
}

wxString ConverterHelper::intToWxString(int integer)
{
	return wxString::Format(wxT("%i"), integer);
}

int ConverterHelper::stringToInt(std::string inputString)
{
	return std::stoi(inputString);
}

std::string ConverterHelper::wxStringToString(wxString inputString)
{
	return inputString.ToStdString();
}

wxString ConverterHelper::createBirdName(std::string en, std::string gen, std::string sp)
{
	wxString birdName = "";
	birdName.Append(en);
	birdName.Append(" (");
	birdName.Append(gen);
	birdName.Append(" ");
	birdName.Append(sp);
	birdName.Append(")");
	return birdName;
}

wxString ConverterHelper::getEnFromBirdName(wxString birdName)
{
	return birdName.SubString(0, birdName.find("(")-1).Trim(true);
}

wxString ConverterHelper::getGenFromBirdName(wxString birdName)
{
	wxString genAndSp = getGenAndSpFromBirdName(birdName);
	return genAndSp.SubString(0, genAndSp.find(" ") - 1);
}

wxString ConverterHelper::getSpFromBirdName(wxString birdName)
{
	wxString genAndSp = getGenAndSpFromBirdName(birdName);
	return genAndSp.SubString(genAndSp.find(" ") + 1, genAndSp.Length()-1);
}

wxString ConverterHelper::getGenAndSpFromBirdName(wxString birdName)
{
	return birdName.SubString(birdName.find("(") + 1, birdName.find(")") - 1);
}

