
#include <wx/wx.h>
#include <wx/listctrl.h>
#include "ApiConnector.h"
#include "MediaCtrlWrapper.h"
#include <wx/url.h>
#include <chrono>
#include <iostream>
#include <ctime>
#include <fstream>
#include <wx/stream.h>
class VogelQuietschGame
{
public:
	VogelQuietschGame(wxButton* StartGameButton, wxButton* confirmSelectionButton, wxButton* DontKnowButton, wxButton* StopGameButton, wxButton* ShowRecOnXenoCantoButton, wxButton* ShowBirdOnDuckDuckGoButton,
		wxListCtrl* activeList, wxListCtrl* gameList,
		wxStaticText* BirdInfoIDText, wxStaticText* BirdInfoGenText, wxStaticText* BirdInfoSpText, wxStaticText* BirdInfoSspText,
		wxStaticText* BirdInfoNameText,	wxStaticText* BirdInfoCountryText, wxStaticText* BirdInfoTypeText, wxStaticText* BirdInfoLocationText,
		wxStaticText* BirdInfoDateText, wxStaticText* BirdInfoRmkText, wxStaticText* BirdInfoBirdSeenText, wxStaticText* BirdInfoUsedPlaybackText,
		wxStaticText* BirdInfoRecText, wxStaticText* BirdInfoQualityText, wxStaticText* BirdInfoNumRecText,	wxStaticText* BirdInfoOthersText, wxStaticText* QuizzInfoText, wxStaticText* QuizzInfoText2,
		wxCheckBox* SongCheckBox, wxCheckBox* CallCheckBox, wxCheckBox* HighQualityCheckBox, wxCheckBox* SonoCheckBox, wxChoice* NumberOfTriesChoice,
		wxStaticBitmap* SonoBitmap, wxTextCtrl* m_textCtrl_BirdSelection,
		MediaCtrlWrapper* mediaCtrlWrapper);
	~VogelQuietschGame();



private:


	void onConfirmSelectionButton(wxCommandEvent& event);
	void onGameBirdSelection(wxListEvent& event);
	void onStartQuizz(wxCommandEvent& event);
	void onStopQuizz(wxCommandEvent& event);
	void onDontKnow(wxCommandEvent& event);
	void onSonoCheckbox(wxCommandEvent& event);
	void onSearchBird(wxCommandEvent& event);
	void StartGame();
	void StopGame();
	void PrepareForNextRecording();
	wxString ChooseRandomBird();
	void PlayNextBirdRecording();
	void ShowRecordingInfo();
	void ResetRecordingInfo();
	void SaveHistoryEntry();
	const char* recordingsDB = "recordings.db";
	const char* historyDB = "history.db";

	wxListCtrl* activeList;
	wxListCtrl* gameList;
	ApiConnector* apiConnector;
	MediaCtrlWrapper* mediaCtrlWrapper;
	
	wxButton* startGameButton;
	wxButton* confirmSelectionButton;


	/// <summary>
	/// Game Data
	/// </summary>
	wxString actualBird;
	wxString selectedBird;
	wxString numRecordings;
	Json::Value actualRecording;
	Json::Value history;
	Json::Value currentRecordingInfo;
	int tryCounter;
	int maxTries;
	boolean guessedCorrectly;
	boolean pressedDontKnow;
	int sessionId;
	wxArrayString activeBirdList;
	wxStaticText* BirdInfoIDText;
	wxStaticText* BirdInfoGenText;
	wxStaticText* BirdInfoSpText;
	wxStaticText* BirdInfoSspText;
	wxStaticText* BirdInfoNameText;
	wxStaticText* BirdInfoCountryText;
	wxStaticText* BirdInfoTypeText;
	wxStaticText* BirdInfoLocationText;
	wxStaticText* BirdInfoDateText;
	wxStaticText* BirdInfoRmkText;
	wxStaticText* BirdInfoBirdSeenText;
	wxStaticText* BirdInfoUsedPlaybackText;
	wxStaticText* BirdInfoRecText;
	wxStaticText* BirdInfoQualityText;
	wxStaticText* BirdInfoNumRecText;
	wxStaticText* BirdInfoOthersText;
	wxStaticText* QuizzInfoText;
	wxStaticText* QuizzInfoText2;
	wxCheckBox* SongCheckBox;
	wxCheckBox* CallCheckBox;
	wxCheckBox* HighQualityCheckBox;
	wxCheckBox* SonoCheckBox;
	wxButton* DontKnowButton;
	wxButton* StopGameButton;
	wxButton* ShowRecOnXenoCantoButton;
	wxButton* ShowBirdOnDuckDuckGoButton;
	wxChoice* NumberOfTriesChoice;
	wxStaticBitmap* SonoBitmap;
	wxTextCtrl* BirdSelectionTextCtrl;

};
