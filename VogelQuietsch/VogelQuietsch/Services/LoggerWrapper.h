#include <plog/Log.h>
#include "plog/Initializers/RollingFileInitializer.h"
#include <plog/Appenders/DebugOutputAppender.h>
class LoggerWrapper
{
	
public:
	LoggerWrapper();
	void initializeLogger(plog::Severity logLevel);
};
