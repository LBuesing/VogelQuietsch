#include <sqlite3.h>
#include <stdio.h>
#include <iostream>
#include <json/json.h>
#include <regex>
#include <wx/wx.h>
#include <chrono>
class SQLiteWrapper
{

public:
	static int createDB(const char* s);
	static int createTableHistoryDB(const char* fileName);
	static int createTableRecordingsDB(const char* s);
	static int deleteData(const char* s);
	
	static int insertData(const char* s, std::string, sqlite3* DB);
	static int updateData(const char* s);
	static int selectData(const char* s);
	static int callback(char* recording, int argc, char** argv, char** azColName);
	static std::string createRecordingInsertQuery(Json::Value recordingInfo);
	static void addHistoryEntry(const char* fileName, Json::Value historyEntry);
	static void addRecordingInfo(const char* fileName, Json::Value jsonValue);
	static void addMultipleRecordingInfo(const char* fileName, Json::Value jsonValue);

	//static Json::Value getRecordings(const char* fileName, wxString birdName, boolean call, boolean song, boolean highQuality);

	static Json::Value getRecordings(const char* fileName, wxString birdName, boolean call, boolean song, boolean highQuality);

	static boolean checkForOldRecordings(const char* fileName, wxString birdName, boolean call, boolean song, boolean highQuality, int maxAgeInSeconds);

};
	