
#include "VogelQuietschGame.h"
#include "LoggerWrapper.h"
#include "HelperFunctions.h"
#include "SQLiteWrapper.h"
VogelQuietschGame::VogelQuietschGame(wxButton* startGameButton, wxButton* confirmSelectionButton, wxButton* DontKnowButton, wxButton* StopGameButton, wxButton* ShowRecOnXenoCantoButton, wxButton* ShowBirdOnDuckDuckGoButton, wxListCtrl* activeList, wxListCtrl* gameList,
    wxStaticText* BirdInfoIDText, wxStaticText* BirdInfoGenText, wxStaticText* BirdInfoSpText, wxStaticText* BirdInfoSspText,
    wxStaticText* BirdInfoNameText, wxStaticText* BirdInfoCountryText, wxStaticText* BirdInfoTypeText, wxStaticText* BirdInfoLocationText,
    wxStaticText* BirdInfoDateText, wxStaticText* BirdInfoRmkText, wxStaticText* BirdInfoBirdSeenText, wxStaticText* BirdInfoUsedPlaybackText,
    wxStaticText* BirdInfoRecText, wxStaticText* BirdInfoQualityText, wxStaticText* BirdInfoNumRecText, wxStaticText* BirdInfoOthersText, wxStaticText* QuizzInfoText, wxStaticText* QuizzInfoText2,
    wxCheckBox* SongCheckBox, wxCheckBox* CallCheckBox, wxCheckBox* HighQualityCheckBox, wxCheckBox* SonoCheckBox, wxChoice* NumberOfTriesChoice,
    wxStaticBitmap* SonoBitmap, wxTextCtrl* BirdSelectionTextCtrl, MediaCtrlWrapper* mediaCtrlWrapper)
{
    wxInitAllImageHandlers();
    this->activeList = activeList;
    this->gameList = gameList;
    this->apiConnector = new ApiConnector();
    this->mediaCtrlWrapper = mediaCtrlWrapper;
    this->startGameButton = startGameButton;
    this->confirmSelectionButton = confirmSelectionButton;
    this->BirdSelectionTextCtrl = BirdSelectionTextCtrl;

    this->BirdInfoIDText = BirdInfoIDText;
    this->BirdInfoGenText = BirdInfoGenText;
    this->BirdInfoSpText = BirdInfoSpText;
    this->BirdInfoSspText = BirdInfoSspText;
    this->BirdInfoNameText = BirdInfoNameText;
    this->BirdInfoCountryText = BirdInfoCountryText;
    this->BirdInfoTypeText = BirdInfoTypeText;
    this->BirdInfoLocationText = BirdInfoLocationText;
    this->BirdInfoDateText = BirdInfoDateText;
    this->BirdInfoRmkText = BirdInfoRmkText;
    this->BirdInfoBirdSeenText = BirdInfoBirdSeenText;
    this->BirdInfoUsedPlaybackText = BirdInfoUsedPlaybackText;
    this->BirdInfoRecText = BirdInfoRecText;
    this->BirdInfoQualityText = BirdInfoQualityText;
    this->BirdInfoNumRecText = BirdInfoNumRecText;
    this->BirdInfoOthersText = BirdInfoOthersText;
    this->QuizzInfoText = QuizzInfoText;
    this->QuizzInfoText2 = QuizzInfoText2;
    this->DontKnowButton = DontKnowButton;
    this->StopGameButton = StopGameButton;
    this->ShowRecOnXenoCantoButton = ShowRecOnXenoCantoButton;
    this->ShowBirdOnDuckDuckGoButton = ShowBirdOnDuckDuckGoButton;

    this->confirmSelectionButton->Disable();
    this->gameList->Bind(wxEVT_LIST_ITEM_SELECTED, &VogelQuietschGame::onGameBirdSelection, this);
    this->confirmSelectionButton->Bind(wxEVT_BUTTON, &VogelQuietschGame::onConfirmSelectionButton, this);
    this->startGameButton->Bind(wxEVT_BUTTON, &VogelQuietschGame::onStartQuizz, this);
    this->DontKnowButton->Bind(wxEVT_BUTTON, &VogelQuietschGame::onDontKnow, this);
    this->StopGameButton->Bind(wxEVT_BUTTON, &VogelQuietschGame::onStopQuizz, this);
   
    this->SonoBitmap = SonoBitmap;
    
    this->CallCheckBox = CallCheckBox;
    this->SongCheckBox = SongCheckBox;
    this->HighQualityCheckBox = HighQualityCheckBox;
    this->SonoCheckBox = SonoCheckBox;

    this->SonoCheckBox->Bind(wxEVT_CHECKBOX, &VogelQuietschGame::onSonoCheckbox, this);
    this->NumberOfTriesChoice = NumberOfTriesChoice;
    tryCounter = 1;
    maxTries = wxAtoi(this->NumberOfTriesChoice->GetString(this->NumberOfTriesChoice->GetSelection()));
    sessionId = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();

    this->BirdSelectionTextCtrl->Bind(wxEVT_TEXT, &VogelQuietschGame::onSearchBird, this);

    SQLiteWrapper::createDB(recordingsDB);
    SQLiteWrapper::createTableRecordingsDB(recordingsDB);
    SQLiteWrapper::createDB(historyDB);
    SQLiteWrapper::createTableHistoryDB(historyDB);

}

VogelQuietschGame::~VogelQuietschGame()
{
   delete apiConnector;
  
}

void VogelQuietschGame::StartGame()
{
    if (activeList->GetItemCount() == 0) {
        wxMessageBox("Active list is empty! Please place birds in the active list.");
        return;
    }
    
    maxTries = wxAtoi(NumberOfTriesChoice->GetString(NumberOfTriesChoice->GetSelection()));
    NumberOfTriesChoice->Disable();
    DontKnowButton->Enable();
    startGameButton->Disable();
    BirdSelectionTextCtrl->SetValue("");
    startGameButton->SetLabelText("Weiter");
    QuizzInfoText->SetLabelText("");
    QuizzInfoText2->SetLabelText("Daten werden geladen...");
    HelperFunctions::SyncLists(activeList, gameList);
    activeBirdList = HelperFunctions::GetListEntries(activeList);
    PlayNextBirdRecording();
    QuizzInfoText2->SetLabelText("Versuche �brig: " + ConverterHelper().intToWxString(maxTries));
}



void VogelQuietschGame::StopGame()
{
}

void VogelQuietschGame::PrepareForNextRecording()
{
    mediaCtrlWrapper->Stop();
    confirmSelectionButton->Disable();
    DontKnowButton->Disable();
    startGameButton->Enable();
    ShowRecordingInfo();
    NumberOfTriesChoice->Enable();

    SaveHistoryEntry();
}


wxString VogelQuietschGame::ChooseRandomBird()
{
  
    return gameList->GetItemText((rand() % gameList->GetItemCount()));
}


void VogelQuietschGame::PlayNextBirdRecording()
{
    tryCounter = 1;
    ResetRecordingInfo();
    actualBird = ChooseRandomBird();
    PLOGI << "actual bird is: " << actualBird;
    //QuizzInfoText2->SetLabelText("Daten werden geladen...");

    boolean foundOldRecords = SQLiteWrapper::checkForOldRecordings(recordingsDB, actualBird, CallCheckBox->GetValue(), SongCheckBox->GetValue(), HighQualityCheckBox->GetValue(), 86400);
    Json::Value recordings;
    if (!foundOldRecords) {
        recordings = SQLiteWrapper::getRecordings(recordingsDB, actualBird, CallCheckBox->GetValue(), SongCheckBox->GetValue(), HighQualityCheckBox->GetValue());
        PLOGI << "Found " << recordings.size() << " recordings";
    }
    
    
   
    if (recordings.size() == 0 || foundOldRecords) {
        std::stringstream converter;
        converter << std::boolalpha << foundOldRecords;
        PLOGI << "No records found in db, or old records found. Checking Xeno-Canto. Found old records: "<< converter.str();
        recordings = apiConnector->getRecordings(actualBird, CallCheckBox->GetValue(), SongCheckBox->GetValue(), HighQualityCheckBox->GetValue());
        int recordCountXenoCanto = recordings.size();
        if (recordCountXenoCanto == 0) {
            wxMessageBox("No records found!");
            PLOGI << "No records found!";
            confirmSelectionButton->Disable();
            DontKnowButton->Disable();
            startGameButton->Enable();
            return;
        }


        SQLiteWrapper::addMultipleRecordingInfo(recordingsDB, recordings);
        PLOGI << "Found " << recordings.size() << " recordings";
        recordings = SQLiteWrapper::getRecordings(recordingsDB, actualBird, CallCheckBox->GetValue(), SongCheckBox->GetValue(), HighQualityCheckBox->GetValue());
        PLOGI << "Found " << recordings.size() << " recordings";
        if (recordings.size() == 0) {
            if (recordCountXenoCanto > 0) {
                wxMessageBox("No records found for "+ actualBird +" in local DB! There are "+ std::to_string(recordCountXenoCanto) +" records in the Xeno-Canto DB, but with your parameters(call, song, quality). Change parameters or remove bird from active list for this game.");
            }
            else {
                wxMessageBox("No records found for " + actualBird + "!");
            }
            
            PLOGI << "No records found!";
            confirmSelectionButton->Disable();
            DontKnowButton->Disable();
            startGameButton->Enable();
            return;
        }
    }
    //Json::StreamWriterBuilder builder;
    //
    //PLOGI << Json::writeString(builder, recordings);
    actualRecording = recordings[(rand() % recordings.size())];
    
    numRecordings = wxString::Format(wxT("%i"), recordings.size());
    wxString soundUrl = actualRecording["file"].asString();
    wxString sonoUrl = "https:"+actualRecording["sono"]["large"].asString();
    PLOGI << "playing " << soundUrl;
    mediaCtrlWrapper->PlayMedia(soundUrl, true);

    PLOGI << "Trying to load sono: " << sonoUrl;

    wxImage sonoImage = apiConnector->curlImage(sonoUrl);
    if (!sonoImage.IsSameAs(wxImage())) {
        sonoImage.Rescale(SonoBitmap->GetSize().GetWidth(), SonoBitmap->GetSize().GetHeight());
        SonoBitmap->SetBitmap(wxBitmap(sonoImage));
    }  
    //QuizzInfoText2->SetLabelText("Versuche �brig: " + ConverterHelper().intToWxString(maxTries - tryCounter));
}

void VogelQuietschGame::ShowRecordingInfo()
{
    BirdInfoIDText->SetLabelText(actualRecording["id"].asString()+", "+ actualRecording["q"].asString()+", "+ numRecordings);
    BirdInfoGenText->SetLabelText(actualRecording["gen"].asString()+" / " + actualRecording["sp"].asString() +" / "+ actualRecording["ssp"].asString());
 /*   BirdInfoSpText->SetLabelText(actualRecording["sp"].asString());
    BirdInfoSspText->SetLabelText(actualRecording["ssp"].asString());*/
    BirdInfoNameText->SetLabelText(actualRecording["en"].asString());
    BirdInfoCountryText->SetLabelText(actualRecording["cnt"].asString());
    BirdInfoTypeText->SetLabelText(actualRecording["type"].asString()+" / "+ actualRecording["sex"].asString()+" / " + actualRecording["stage"].asString());
    BirdInfoLocationText->SetLabelText(actualRecording["loc"].asString());
    BirdInfoLocationText->Wrap(250);
    BirdInfoDateText->SetLabelText(actualRecording["date"].asString()+", "+ actualRecording["time"].asString());
    BirdInfoRmkText->SetLabelText(actualRecording["rmk"].asString());
    BirdInfoRmkText->Wrap(250);
    BirdInfoBirdSeenText->SetLabelText(actualRecording["bird-seen"].asString());
    BirdInfoUsedPlaybackText->SetLabelText(actualRecording["playback-used"].asString());
    BirdInfoRecText->SetLabelText(actualRecording["rec"].asString());
    //BirdInfoQualityText->SetLabelText(actualRecording["q"].asString());
    //BirdInfoNumRecText->SetLabelText(numRecordings);
    wxString others = "";
    for (unsigned int i = 0; i < actualRecording["also"].size(); i++) {
        others.Append(actualRecording["also"][i].asString());
        if (i < actualRecording["also"].size() - 1) {
            others.Append(", ");
        }
    }
    BirdInfoOthersText->SetLabelText(others);
    BirdInfoOthersText->Wrap(250);
}
void VogelQuietschGame::ResetRecordingInfo()
{
    BirdInfoIDText->SetLabelText("");
    BirdInfoGenText->SetLabelText("");
    /*   BirdInfoSpText->SetLabelText(actualRecording["sp"].asString());
       BirdInfoSspText->SetLabelText(actualRecording["ssp"].asString());*/
    BirdInfoNameText->SetLabelText("");
    BirdInfoCountryText->SetLabelText("");
    BirdInfoTypeText->SetLabelText("");
    BirdInfoLocationText->SetLabelText("");
    BirdInfoLocationText->Wrap(250);
    BirdInfoDateText->SetLabelText("");
    BirdInfoRmkText->SetLabelText("");
    BirdInfoRmkText->Wrap(250);
    BirdInfoBirdSeenText->SetLabelText("");
    BirdInfoUsedPlaybackText->SetLabelText("");
    BirdInfoRecText->SetLabelText("");
    //BirdInfoQualityText->SetLabelText(actualRecording["q"].asString());
    //BirdInfoNumRecText->SetLabelText(numRecordings);
 
    BirdInfoOthersText->SetLabelText("");
    BirdInfoOthersText->Wrap(250);
}

void VogelQuietschGame::SaveHistoryEntry() {
    currentRecordingInfo["birdName"] = ConverterHelper().wxStringToString(actualBird);
    currentRecordingInfo["tryCounter"] = tryCounter;
    currentRecordingInfo["maxTries"] = maxTries;
    currentRecordingInfo["guessedCorrectly"] = (guessedCorrectly) ? 1 : 0;
    currentRecordingInfo["pressedDontKnow"] = (pressedDontKnow) ? 1 : 0;
    currentRecordingInfo["sessionId"] = sessionId;
    currentRecordingInfo["recording"] = actualRecording;
    SQLiteWrapper::addHistoryEntry(historyDB, currentRecordingInfo);
    /*StorageService().appendJsonToTxtFile("fullHistory.json", currentRecordingInfo);
    history.append(currentRecordingInfo);
    PLOGD << currentRecordingInfo["birdName"].asString() << currentRecordingInfo["guessedCorrectly"].asString() << history.size();*/
}

void VogelQuietschGame::onConfirmSelectionButton(wxCommandEvent& event)
{

    if (selectedBird.IsSameAs(actualBird)) {
        PLOGI << "Correct bird was selected: " << selectedBird;

        guessedCorrectly = true;
        pressedDontKnow = false;
        QuizzInfoText->SetFont(QuizzInfoText->GetFont().MakeBold());
        QuizzInfoText->GetFont().SetPointSize(30);
        QuizzInfoText->SetLabelText("RICHTIG!");
        QuizzInfoText2->SetLabelText("Versuche: "+ ConverterHelper().intToWxString(tryCounter));
        PrepareForNextRecording();
    }
    else {
        PLOGI << "Wrong bird selected: " << selectedBird << " Correct bird was: " << actualBird;
        
        guessedCorrectly = false;
        pressedDontKnow = false;
        QuizzInfoText->SetLabelText("FALSCH!");
        currentRecordingInfo["wronglyGuessed"].append(ConverterHelper().wxStringToString(selectedBird));
        if (tryCounter == maxTries) {
            PrepareForNextRecording();
        }
        if (maxTries - tryCounter == 1) {
            QuizzInfoText2->SetLabelText("Letzter Versuch \nU ken do it!");
        }
        else {
            QuizzInfoText2->SetLabelText("Versuche �brig: " + ConverterHelper().intToWxString(maxTries - tryCounter));
        }
      
         tryCounter += 1;

    }
    BirdSelectionTextCtrl->SetValue("");
    
    
    
}

void VogelQuietschGame::onGameBirdSelection(wxListEvent& event)
{
    selectedBird = gameList->GetItemText(event.GetIndex());
    if (!startGameButton->IsEnabled()) {
        confirmSelectionButton->Enable();
    }
 
  
    
}
void VogelQuietschGame::onStartQuizz(wxCommandEvent& event)
{

    StartGame();

}

void VogelQuietschGame::onStopQuizz(wxCommandEvent& event)
{
    StopGame();
    mediaCtrlWrapper->Stop();
    startGameButton->Enable();
    startGameButton->SetLabelText("Quizz starten");
    //PLOGD << "size of hist: " << history.size();
    //Json::StreamWriterBuilder builder;
    //builder["indentation"] = ""; // If you want whitespace-less output
    //const std::string output = Json::writeString(builder, history);
    //wxString wxOutput(output);
    //StorageService().writeTxtFile("lastSession.json", wxOutput);
}

void VogelQuietschGame::onDontKnow(wxCommandEvent& event)
{
    
    PLOGI << "Sad! Correct bird was: " << actualBird;
    guessedCorrectly = false;
    pressedDontKnow = true;
    QuizzInfoText->SetLabelText("SCHADE!");
    BirdSelectionTextCtrl->SetValue("");
    PrepareForNextRecording();
    
}

void VogelQuietschGame::onSearchBird(wxCommandEvent& event) {
    
    HelperFunctions::regexBirdList(gameList, activeBirdList, BirdSelectionTextCtrl->GetValue());

}

void VogelQuietschGame::onSonoCheckbox(wxCommandEvent& event)
{
    SonoBitmap->Show(SonoCheckBox->GetValue());
}
