#pragma once

#include <wx/mediactrl.h>
#include <wx/wx.h>
#include "wx/timer.h"       // timer for updating status bar

class wxMediaPlayerTimer : public wxTimer
{
public:
	// Ctor
	wxMediaPlayerTimer(wxMediaCtrl* mediaCtrl, wxSlider* TimeSlider, boolean* m_bdoNotify, wxStaticText* m_staticText_Time);

	// Called each time the timer's timeout expires
	void Notify() wxOVERRIDE;
private:
	wxMediaCtrl* mediaCtrl;
	wxSlider* TimeSlider;
	wxStaticText* TimeText;
	void OnBeginSeek(wxScrollEvent& WXUNUSED(event));
	void OnEndSeek(wxScrollEvent& WXUNUSED(event));
	boolean m_bIsBeingDragged;
	boolean IsBeingDragged();
	boolean* m_bdoNotify;
	boolean doNotify();
	
	
};
class MediaCtrlWrapper{
public:
	MediaCtrlWrapper(wxWindow* parent, wxSlider* TimeSlider, wxStaticText* m_staticText_Time, wxButton* m_button_Play, wxButton* m_button_Pause, wxButton* m_button_Stop);
	void PlayMedia(const wxString& path, boolean useControls);
	void PlayWarning();
	void Stop();
	void Pause();
	void Play();
	void SetVolume(double volume);
	double GetVolume();
	void SetPosition(double position);
	double GetPosition();
	~MediaCtrlWrapper();
	wxMediaCtrl* mediaCtrl;
private:
	void OnMediaLoaded(wxMediaEvent& event);
	wxMediaPlayerTimer* timer;
	boolean* m_bdoNotify;
	void setDoNotify(boolean doNotify);
	wxButton* PlayButton;
	wxButton* PauseButton;
	wxButton* StopButton;
	void onPlay(wxCommandEvent& event);
	void onPause(wxCommandEvent& event);
	void onStop(wxCommandEvent& event);

};




