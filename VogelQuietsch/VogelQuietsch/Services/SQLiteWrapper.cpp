
#include "SQLiteWrapper.h"
#include "LoggerWrapper.h"
#include <ConverterHelper.h>
int SQLiteWrapper::createDB(const char* s)
{
	sqlite3* DB;

	int exit = 0;
	exit = sqlite3_open(s, &DB);

	sqlite3_close(DB);

	return 0;
}
int SQLiteWrapper::createTableHistoryDB(const char* fileName)
{
	sqlite3* DB;
	char* messageError;

	std::string sql = "CREATE TABLE IF NOT EXISTS RECORDINGS("
		"ID INTEGER PRIMARY KEY AUTOINCREMENT, "
		"BIRDNAME			TEXT, "
		"TRY_COUNTER		INTEGER, "
		"MAX_TRIES			INTEGER, "
		"GUESSED_CORRECTLY	INTEGER, "
		"PRESSED_DONT_KNOW	INTEGER, "
		"WRONGLY_GUESSED    JSON, "
		"SESSION_ID			INTEGER, "
		"TIMESTAMP			INTEGER, "
		"TYPE				TEXT, "
		"RECORDING  JSON);";


	try
	{
		PLOGI << "try to create table in " << fileName;
		int exit = 0;
		exit = sqlite3_open(fileName, &DB);
		/* An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here */
		exit = sqlite3_exec(DB, sql.c_str(), NULL, 0, &messageError);
		if (exit != SQLITE_OK) {
			PLOGE << "Error in createTable function: " << messageError;
			sqlite3_free(messageError);
		}
		else
			PLOGI << "Table created Successfully";
		sqlite3_close(DB);
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what();
	}

	return 0;
}
int SQLiteWrapper::createTableRecordingsDB(const char* fileName)
{
	sqlite3* DB;
	char* messageError;

	std::string sql = "CREATE TABLE IF NOT EXISTS RECORDINGS("
		"ID INTEGER PRIMARY KEY AUTOINCREMENT, "
		"REC_ID 	INTEGER, "
		"TIMESTAMP  INTEGER, "
		"BIRDNAME	TEXT, "
		"EN			TEXT, "
		"GEN		TEXT, "
		"SP			TEXT, "
		"SSP		TEXT, "
		"TYPE		TEXT, "
		"SEX		TEXT, "
		"LENGTH		TEXT, "
		"Q			TEXT, "
		"STAGE		TEXT,"
		"RECORDING  JSON,"
		"UNIQUE(REC_ID)); ";

	/*std::string sql = "CREATE TABLE IF NOT EXISTS GRADES("
		"ID INTEGER PRIMARY KEY AUTOINCREMENT, "
		"NAME      TEXT NOT NULL, "
		"LNAME     TEXT NOT NULL, "
		"AGE       INT  NOT NULL, "
		"ADDRESS   CHAR(50), "
		"GRADE     CHAR(1) );";*/
	
	try
	{
		PLOGI << "try to create table in " << fileName;
		int exit = 0;
		exit = sqlite3_open(fileName, &DB);
		/* An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here */
		exit = sqlite3_exec(DB, sql.c_str(), NULL, 0, &messageError);
		if (exit != SQLITE_OK) {
			PLOGE << "Error in createTable function: " <<messageError;
			sqlite3_free(messageError);
		}
		else
			PLOGI << "Table created Successfully";
		sqlite3_close(DB);
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what();
	}

	return 0;
}

int SQLiteWrapper::insertData(const char* s, std::string sql, sqlite3* DB)
{
	//sqlite3* DB;
	char* messageError;


	/* An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here */
	int exit = sqlite3_exec(DB, sql.c_str(), NULL, 0, &messageError);
	if (exit != SQLITE_OK) {
		PLOGE << "Error in insertData function." << messageError;
		PLOGE << sql;
		sqlite3_free(messageError);
	}
	else
		PLOGI << "Records inserted Successfully!";
	//sqlite3_close(DB);
	return 0;
}

int SQLiteWrapper::updateData(const char* s)
{
	sqlite3* DB;
	char* messageError;

	std::string sql("UPDATE GRADES SET GRADE = 'A' WHERE LNAME = 'Cooper'");

	int exit = sqlite3_open(s, &DB);
	/* An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here */
	exit = sqlite3_exec(DB, sql.c_str(), NULL, 0, &messageError);
	if (exit != SQLITE_OK) {
		PLOGE << "Error in updateData function." << messageError;
		sqlite3_free(messageError);
	}
	else
		std::cout << "Records updated Successfully!" << std::endl;

	return 0;
}

int SQLiteWrapper::deleteData(const char* s)
{
	//sqlite3* DB;
	//char* messageError;

	//std::string sql = "DELETE FROM GRADES;";

	//int exit = sqlite3_open(s, &DB);
	///* An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here */
	//exit = sqlite3_exec(DB, sql.c_str(), callback, NULL, &messageError);
	//if (exit != SQLITE_OK) {
	//	std::cerr << "Error in deleteData function." << std::endl;
	//	sqlite3_free(messageError);
	//}
	//else
	//	std::cout << "Records deleted Successfully!" << std::endl;

	return 0;
}

int SQLiteWrapper::selectData(const char* s)
{
	//sqlite3* DB;
	//char* messageError;

	//std::string sql = "SELECT * FROM GRADES;";

	//int exit = sqlite3_open(s, &DB);
	///* An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here*/
	//exit = sqlite3_exec(DB, sql.c_str(), callback, NULL, &messageError);

	//if (exit != SQLITE_OK) {
	//	PLOGE << "Error in selectData function." << messageError;
	//	sqlite3_free(messageError);
	//}
	//else
	//	std::cout << "Records selected Successfully!" << std::endl;

	return 0;
}

// retrieve contents of database used by selectData()
/* argc: holds the number of results, argv: holds each value in array, azColName: holds each column returned in array, */
int SQLiteWrapper::callback(char* recording, int argc, char** argv, char** azColName)
{

	//for (int i = 0; i < argc; i++) {
	//	// column name and value
	//	std::cout << azColName[i] << ": " << argv[i] << std::endl;
	//}

	//std::cout << std::endl;

	//return 0;
	recording = argv[0];

	int idx;

	printf("There are %d column(s)\n", argc);

	for (idx = 0; idx < argc; idx++) {
		printf("The data in column \"%s\" is: %s\n", azColName[idx], argv[idx]);
	}

	printf("\n");

	return 0;
}

std::string SQLiteWrapper::createRecordingInsertQuery(Json::Value recordingInfo) {
	ConverterHelper converterHelper = ConverterHelper();
	std::string id = recordingInfo["id"].asString();
	int timeStamp = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	std::string birdName = std::regex_replace(converterHelper.wxStringToString(converterHelper.createBirdName(recordingInfo["en"].asString(), recordingInfo["gen"].asString(), recordingInfo["sp"].asString())), std::regex("'"), "''");
	std::string en = std::regex_replace(recordingInfo["en"].asString(), std::regex("'"), "''");
	std::string gen = std::regex_replace(recordingInfo["gen"].asString(), std::regex("'"), "''");
	std::string sp = std::regex_replace(recordingInfo["sp"].asString(), std::regex("'"), "''");
	std::string ssp = std::regex_replace(recordingInfo["ssp"].asString(), std::regex("'"), "''");
	std::string type = std::regex_replace(recordingInfo["type"].asString(), std::regex("'"), "''");
	std::string sex = std::regex_replace(recordingInfo["sex"].asString(), std::regex("'"), "''");
	std::string length = std::regex_replace(recordingInfo["length"].asString(), std::regex("'"), "''");
	std::string q = std::regex_replace(recordingInfo["q"].asString(), std::regex("'"), "''");
	std::string stage = std::regex_replace(recordingInfo["stage"].asString(), std::regex("'"), "''");
	Json::StreamWriterBuilder builder;
	builder["indentation"] = ""; // If you want whitespace-less output
	std::string recordingInfoString = std::regex_replace(Json::writeString(builder, recordingInfo), std::regex("'"), "''");


	/*std::string sql("INSERT OR IGNORE INTO RECORDINGS (REC_ID, TIMESTAMP, BIRDNAME, EN, GEN, SP, SSP, TYPE, SEX, LENGTH, Q, STAGE, RECORDING)"
		"VALUES('" + id + "','" + std::to_string(timeStamp) + "','" + birdName + "','" + en + "','" + gen + "','" + sp + "','" + ssp + "','" + type + "','" + sex + "','" + length + "','" + q + "','" + stage + "','" + std::regex_replace(recordingInfoString, std::regex("'"), "''") + "');");*/

	std::string sql("INSERT INTO RECORDINGS (REC_ID, TIMESTAMP, BIRDNAME, EN, GEN, SP, SSP, TYPE, SEX, LENGTH, Q, STAGE, RECORDING)"
		"VALUES(" + id + "," + std::to_string(timeStamp) + ",'" + birdName + "','" + en + "','" + gen + "','" + sp + "','" + ssp + "','" + type + "','" + sex + "','" + length + "','" + q + "','" + stage + "','" +recordingInfoString + "') ON CONFLICT(REC_ID) DO UPDATE SET "
		"TIMESTAMP=" + std::to_string(timeStamp) + ", "
		"BIRDNAME='" + birdName + "', "
		"EN='" + en + "', "
		"GEN='" + gen + "', "
		"SP='" + sp + "', "
		"SSP='" + ssp + "', "
		"TYPE='" + type + "', "
		"SEX='" + sex + "', "
		"LENGTH='" + length + "', "
		"Q='" + q + "', "
		"STAGE='" + stage + "', "
		"RECORDING='" + recordingInfoString + "';");
	return sql;
}
void SQLiteWrapper::addHistoryEntry(const char* fileName, Json::Value historyEntry)
{

	ConverterHelper converterHelper = ConverterHelper();
	int timeStamp = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	std::string birdName = historyEntry["birdName"].asString();
	std::string tryCounter = std::regex_replace(historyEntry["tryCounter"].asString(), std::regex("'"), "''");
	std::string maxTries = std::regex_replace(historyEntry["maxTries"].asString(), std::regex("'"), "''");
	std::string guessedCorrectly = std::regex_replace(historyEntry["guessedCorrectly"].asString(), std::regex("'"), "''");
	std::string pressedDontKnow = std::regex_replace(historyEntry["pressedDontKnow"].asString(), std::regex("'"), "''");

	std::string sessionId = std::regex_replace(historyEntry["sessionId"].asString(), std::regex("'"), "''");
	std::string type = std::regex_replace(historyEntry["recording"]["type"].asString(), std::regex("'"), "''");

	Json::StreamWriterBuilder builder;
	builder["indentation"] = ""; // If you want whitespace-less output
	std::string recordingInfoString = std::regex_replace(Json::writeString(builder, historyEntry["recording"]), std::regex("'"), "''");
	std::string wronglyGuessed = std::regex_replace(Json::writeString(builder, historyEntry["wronglyGuessed"]), std::regex("'"), "''");

	std::string sql("INSERT INTO RECORDINGS (BIRDNAME, TRY_COUNTER, MAX_TRIES, GUESSED_CORRECTLY, PRESSED_DONT_KNOW, WRONGLY_GUESSED, SESSION_ID, TIMESTAMP, TYPE, RECORDING)"
		"VALUES('" + birdName + "'," + tryCounter + "," + maxTries + "," + guessedCorrectly + "," + pressedDontKnow + ",'" + wronglyGuessed + "'," + sessionId + "," + std::to_string(timeStamp) + ",'" + type + "','" + recordingInfoString + "');");
	sqlite3* DB;

	int exit = sqlite3_open(fileName, &DB);

	insertData(fileName, sql, DB);
	sqlite3_close(DB);
}
void SQLiteWrapper::addRecordingInfo(const char* fileName, Json::Value recordingInfo)
{
	
	
	sqlite3* DB;
	
	int exit = sqlite3_open(fileName, &DB);
	std::string sql = createRecordingInsertQuery(recordingInfo);
	insertData(fileName, sql, DB);
	sqlite3_close(DB);
}
void SQLiteWrapper::addMultipleRecordingInfo(const char* fileName, Json::Value recordingInfos)
{
	sqlite3* DB;
	ConverterHelper converterHelper = ConverterHelper();
	int exit = sqlite3_open(fileName, &DB);
	if (exit != SQLITE_OK) {
		PLOGE << "Error in selectData function." ;
	}
	else
		PLOGI << "DB opened successfully!" ;
	sqlite3_exec(DB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
	for (signed int i = 0; i < recordingInfos.size(); i++) {
		Json::Value recordingInfo = recordingInfos[i];
		std::string sql = createRecordingInsertQuery(recordingInfo);
		//PLOGI << "Inserting " << sql ;
		insertData(fileName, sql, DB);
	}
	sqlite3_exec(DB, "END TRANSACTION;", NULL, NULL, NULL);
	sqlite3_close(DB);
}

//Json::Value SQLiteWrapper::getRecordings(const char* fileName, wxString birdName, boolean call, boolean song, boolean highQuality)
//{
//	PLOGI << "Starting sqlite db search";
//	ConverterHelper converterHelper = ConverterHelper();
//	//Add api query parameters to query link
//	wxString apiParameters = "GEN = '";
//	apiParameters.Append(converterHelper.getGenFromBirdName(birdName));
//	apiParameters.Append("' AND SP = '");
//	apiParameters.Append(converterHelper.getSpFromBirdName(birdName));
//	apiParameters.Append("'");
//	if (song && !call)
//	{
//		apiParameters.Append(" AND TYPE = 'song'");
//	}
//	else if (!song && call)
//	{
//		apiParameters.Append(" AND TYPE = 'call'");
//	}
//	if (highQuality)
//	{
//		apiParameters.Append(" AND Q = 'A'");
//	}
//
//	sqlite3* DB;
//	char* messageError;
//	char* recording;
//	std::string sql = "SELECT RECORDING FROM RECORDINGS WHERE "+ converterHelper.wxStringToString(apiParameters) +";";
//	PLOGI << sql;
//	int exit = sqlite3_open(fileName, &DB);
//	/* An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here*/
//
//	//exit = sqlite3_exec(DB, sql.c_str(), callback, &recording, &messageError);
//	PLOGI << recording;
//	if (exit != SQLITE_OK) {
//		PLOGE << "Error in selectData function." << messageError;
//		PLOGE << sql;
//		sqlite3_free(messageError);
//	}
//	else
//		std::cout << "Records selected Successfully!" << std::endl;
//
//	Json::Value json = converterHelper.parseJSON(recording);
//	return json;
//
//}

Json::Value SQLiteWrapper::getRecordings(const char* fileName, wxString birdName, boolean call, boolean song, boolean highQuality)
{
	PLOGI << "Starting sqlite db search";
	ConverterHelper converterHelper = ConverterHelper();
	//Add api query parameters to query link
	wxString apiParameters = "GEN = '";
	apiParameters.Append(converterHelper.getGenFromBirdName(birdName));
	apiParameters.Append("' AND SP = '");
	apiParameters.Append(converterHelper.getSpFromBirdName(birdName));
	apiParameters.Append("'");
	if (song && !call)
	{
		apiParameters.Append(" AND TYPE = 'song'");
	}
	else if (!song && call)
	{
		apiParameters.Append(" AND TYPE = 'call'");
	}
	if (highQuality)
	{
		apiParameters.Append(" AND Q = 'A'");
	}

	sqlite3* DB;
	int exit = sqlite3_open(fileName, &DB);
	//char* messageError;
	/*char* recording;*/
	std::string sql = "SELECT RECORDING FROM RECORDINGS WHERE " + converterHelper.wxStringToString(apiParameters) + ";";
	sqlite3_stmt* stmt;
	int rc = sqlite3_prepare_v2(DB, sql.c_str(), -1, &stmt, NULL);
	if (rc != SQLITE_OK)
		throw std::string(sqlite3_errmsg(DB));


	Json::Value recordings;
	while (sqlite3_step(stmt) == SQLITE_ROW)
	{
		std::string recordingString = std::string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0)));

		//PLOGI << recordingString;
		Json::Value recording = converterHelper.parseJSON(recordingString);
		recordings.append(recording);
	}
	sqlite3_finalize(stmt);
	sqlite3_close(DB);
	return recordings;
}

boolean SQLiteWrapper::checkForOldRecordings(const char* fileName, wxString birdName, boolean call, boolean song, boolean highQuality, int maxAgeInSeconds)
{
	PLOGI << "Starting sqlite db search";
	ConverterHelper converterHelper = ConverterHelper();
	//Add api query parameters to query link
	wxString apiParameters = "GEN = '";
	apiParameters.Append(converterHelper.getGenFromBirdName(birdName));
	apiParameters.Append("' AND SP = '");
	apiParameters.Append(converterHelper.getSpFromBirdName(birdName));
	apiParameters.Append("'");
	if (song && !call)
	{
		apiParameters.Append(" AND TYPE = 'song'");
	}
	else if (!song && call)
	{
		apiParameters.Append(" AND TYPE = 'call'");
	}
	if (highQuality)
	{
		apiParameters.Append(" AND Q = 'A'");
	}
	int currentTimeStamp = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	//PLOGI << "current timeStamp: "<< currentTimeStamp;
	apiParameters.Append(" AND TIMESTAMP < "+ std::to_string(currentTimeStamp - maxAgeInSeconds) +" LIMIT 1");

	sqlite3* DB;
	int exit = sqlite3_open(fileName, &DB);
	//char* messageError;
	/*char* recording;*/
	std::string sql = "SELECT TIMESTAMP FROM RECORDINGS WHERE " + converterHelper.wxStringToString(apiParameters) + ";";
	sqlite3_stmt* stmt;
	int rc = sqlite3_prepare_v2(DB, sql.c_str(), -1, &stmt, NULL);
	if (rc != SQLITE_OK)
		throw std::string(sqlite3_errmsg(DB));


	Json::Value recordings;
	while (sqlite3_step(stmt) == SQLITE_ROW)
	{
		std::string recordingString = std::string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0)));
		PLOGI << "found old recordings: "<< recordingString;
		sqlite3_finalize(stmt);
		sqlite3_close(DB);
		return true;
	}
	sqlite3_finalize(stmt);
	sqlite3_close(DB);

	return false;
}

