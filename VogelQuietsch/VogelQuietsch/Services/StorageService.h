#pragma once
#include <wx/wx.h>
#include <wx/textfile.h>
#include <ConverterHelper.h>

class StorageService
{
public:
	StorageService();
	StorageService(wxString path);
	void setPath(wxString path);
	wxString getPath();
	wxJSONValue readJsonFile(wxString fileName);
	wxArrayString readTxtFile(wxString fileName);
	void appendJsonToTxtFile(wxString fileName, Json::Value jsonValue);
	void writeTxtFile(wxString fileName, wxArrayString stringArray);
	void writeTxtFile(wxString fileName, wxString string);
private:
	wxString path;

};



