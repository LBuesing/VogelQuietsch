
#include "ApiConnector.h"
#include "LoggerWrapper.h"
#include <SQLiteWrapper.h>
ApiConnector::ApiConnector() {
	xenoCantoUrl = "www.xeno-canto.org";
	xenoCantoApiBase = "https://xeno-canto.org/api/2/recordings?query=";
	
}


wxArrayString ApiConnector::searchXenoCanto(wxString name, wxString country, wxString location, wxString other, bool ignoreSsp, bool ignoreSspByRegion, bool loadAllPages, bool limitToRegion, bool addTagCountry, bool addTagLocation, bool addTagOther)
{
	//structlog LOGCFG = {};
	//LOGCFG.headers = false;
	//LOGCFG.level = typelog::DEBUG;
	if (checkUrlAvailability(xenoCantoUrl)) {
		//SQLiteWrapper::createDB("test3.db");
		//SQLiteWrapper::createTableRecordingsDB("test3.db");
		PLOGI << "Starting xeno canto search";
		ConverterHelper converterHelper = ConverterHelper();
		//Add api query parameters to query link
		wxString apiParameters = "";
		apiParameters.Append(name);
		addApiParameter(apiParameters, "cnt:", country);
		addApiParameter(apiParameters, "loc:", location);
		addApiParameter(apiParameters, "", other);
		apiParameters.Replace(" ", "+", true);
		wxString xenoCantoApiQuery = "";
		xenoCantoApiQuery.Append(xenoCantoApiBase).Append(apiParameters);
		
		PLOGI << "Starting curl on api: " << xenoCantoApiQuery;
		wxString curlRes = curlData(xenoCantoApiQuery); 
		PLOGD << "Fixing string";
		//Remove all backslashes so the parser does work.
		curlRes.Replace("\\\"", "'", true);
		curlRes.Replace("\\", "", true);		
		PLOGD << "Converting wxString to string";
		std::string string = converterHelper.wxStringToString(curlRes);
		PLOGD << "Parsing JSON";
		Json::Value json = converterHelper.parseJSON(string);

		wxArrayString birdNames = wxArrayString();
		PLOGD << "Reading birdNames from JSON";
		extractBirdNamesFromJSON(birdNames, json, ignoreSsp, ignoreSspByRegion);

		if (json["numPages"].asInt() > 1 && loadAllPages) {
			for (int i = 2; i <=  json["numPages"].asInt(); i++) {
				PLOGI << "Searching page "<< i << " of " << json["numPages"].asInt();
				wxString xenoCantoApiQueryPage = "";
				xenoCantoApiQueryPage.Append(xenoCantoApiQuery).Append("&page=").Append(converterHelper.intToWxString(i));
				PLOGI << "Starting curl on api: " << xenoCantoApiQuery;
				wxString curlResPaged = curlData(xenoCantoApiQueryPage);
				PLOGD << "Fixing string";
				//Remove all backslashes so the parser does work.
				curlResPaged.Replace("\\\"", "'", true);
				curlResPaged.Replace("\\", "", true);
				PLOGD << "Converting wxString to string";
				std::string stringPaged = converterHelper.wxStringToString(curlResPaged);
				PLOGD << "Parsing JSON";
				Json::Value jsonPaged = converterHelper.parseJSON(stringPaged);
				PLOGD << "Reading birdNames from JSON";
				extractBirdNamesFromJSON(birdNames, jsonPaged, ignoreSsp, ignoreSspByRegion);
			}
		}
		PLOGI << "Finished search";
		return birdNames;
		//StorageService().writeTxtFile("D:\\Users\\lukas\\Dokumente\\VogelQuietsch\\VogelQuietsch\\VogelQuietsch\\birdNames.txt", birdNames);
	}


	return wxArrayString();
}

bool ApiConnector::checkUrlAvailability(wxString urlString)
{
	wxHTTP http;
	if (http.Connect(urlString)) {
		//wxLogMessage(wxT("Successfully reached " +  urlString));
		return true;
	} else {
		wxLogMessage(wxT("Cant reach "  + urlString));
		PLOGW << "Cant reach " << urlString;
	}
		
	return false;
}



//Load txt data from url
wxString ApiConnector::curlData(wxString urlString)
{
	const char* cstr = urlString.c_str();
	CURL* curl;
	CURLcode res;
	std::string readBuffer;
	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, cstr);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
	}

	return wxString(readBuffer);
}
wxImage ApiConnector::curlImage(wxString urlString) {
	const char* cstr = urlString.c_str();
	wxImage image = wxImage();
	CURL* curl = NULL;
	CURLcode res;
	curl = curl_easy_init();
	if (curl) {
		wxMemoryBuffer mem_buf;
		curl_easy_setopt(curl, CURLOPT_URL, cstr);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data_to_wxStream);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &mem_buf);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
		res = curl_easy_perform(curl);
		const char* str = curl_easy_strerror(res);
		curl_easy_cleanup(curl);
		PLOGI << "curl info: " << str;
		PLOGD << "Adding data to stream with length: " << mem_buf.GetDataLen();
		wxMemoryInputStream stream(mem_buf.GetData(), mem_buf.GetDataLen());
		
		image.LoadFile(stream, wxBITMAP_TYPE_ANY);
		
	}
	return image;
}
Json::Value ApiConnector::getRecordings(wxString birdName, boolean call, boolean song, boolean highQuality)
{
	PLOGI << "Starting xeno canto search";
	ConverterHelper converterHelper = ConverterHelper();
	//Add api query parameters to query link
	wxString apiParameters = "";
	apiParameters.Append(converterHelper.getGenAndSpFromBirdName(birdName));
	apiParameters.Replace(" ", "+", true);
	//if (song && !call)
	//{
	//	apiParameters.Append("+type:song");
	//}
	//else if (!song && call)
	//{
	//	apiParameters.Append("+type:call");
	//}
	//if (highQuality)
	//{
	//	apiParameters.Append("+q:A");
	//}
	wxString xenoCantoApiQuery = "";
	xenoCantoApiQuery.Append(xenoCantoApiBase).Append(apiParameters);

	PLOGI << "Starting curl on api: " << xenoCantoApiQuery;
	wxString curlRes = curlData(xenoCantoApiQuery);
	PLOGD << "Fixing string";
	//Remove all backslashes so the parser does work.
	curlRes.Replace("\\\"", "'", true);
	curlRes.Replace("\\", "", true);
	PLOGD << "Converting wxString to string";
	std::string string = converterHelper.wxStringToString(curlRes);
	PLOGD << "Parsing JSON";
	Json::Value json = converterHelper.parseJSON(string);
	Json::Value recordings = json["recordings"];
	if (json["numPages"].asInt() > 1) {
		for (int i = 2; i <= json["numPages"].asInt(); i++) {
			PLOGI << "Searching page " << i << " of " << json["numPages"].asInt();
			wxString xenoCantoApiQueryPage = "";
			xenoCantoApiQueryPage.Append(xenoCantoApiQuery).Append("&page=").Append(converterHelper.intToWxString(i));
			PLOGI << "Starting curl on api: " << xenoCantoApiQuery;
			wxString curlResPaged = curlData(xenoCantoApiQueryPage);
			PLOGD << "Fixing string";
			//Remove all backslashes so the parser does work.
			curlResPaged.Replace("\\\"", "'", true);
			curlResPaged.Replace("\\", "", true);
			PLOGD << "Converting wxString to string";
			std::string stringPaged = converterHelper.wxStringToString(curlResPaged);
			PLOGD << "Parsing JSON";
			Json::Value jsonPaged = converterHelper.parseJSON(stringPaged);
			for (auto rec : jsonPaged["recordings"]) {
				recordings.append(rec);
			}
		}
	}
	return recordings;

}
//function used by curl (DONT TOUCH)
size_t ApiConnector::writeCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}
size_t ApiConnector::write_data_to_wxStream(void* ptr, size_t size, size_t nmemb, void* userdata)
{
	size_t nbytes = size * nmemb;
	((wxMemoryBuffer*)userdata)->AppendData(ptr, nbytes);
	PLOGD << "writing bytes: " << nbytes;
	return nbytes;
}
void ApiConnector::addApiParameter(wxString &apiParameters, wxString parameterKey, wxString parameterValue)
{
	if (!parameterValue.IsEmpty()) {
		if (!apiParameters.IsEmpty()) {
			apiParameters.Append("+");
		}
		apiParameters.Append(parameterKey).Append(parameterValue);
	}
}

void ApiConnector::extractBirdNamesFromJSON(wxArrayString& birdNames, Json::Value json, bool ignoreSsp, bool ignoreSspByRegion)
{
	ConverterHelper converterHelper = ConverterHelper();
	
	wxString oldBirdname = "";
	if (!birdNames.IsEmpty()) {
		oldBirdname = birdNames.Last();
	}
	for (unsigned int i = 0; i < json["recordings"].size(); i++) {
		PLOGV << "Getting birdname " << i << " from JSON";
		
		wxString birdName = converterHelper.createBirdName(json["recordings"][i]["en"].asString(), json["recordings"][i]["gen"].asString(), json["recordings"][i]["sp"].asString());
		//SQLiteWrapper::addRecordingInfo("test3.db", json["recordings"][i]);
		
		if (!birdName.IsSameAs(oldBirdname, true) && !birdName.IsSameAs(" ( )")) {
			PLOGD << "Checked for duplicate birdname and added it to list: " << birdName;
			birdNames.Add(birdName);
			
		}
		oldBirdname = birdName;
	}
	
}



