
#include "LoggerWrapper.h"

LoggerWrapper::LoggerWrapper() {

}

void LoggerWrapper::initializeLogger(plog::Severity logLevel)
{

	static plog::RollingFileAppender<plog::CsvFormatter> fileAppender("VogelQuietschLog.csv", 10000000, 5); // Create the 1st appender.
	static plog::DebugOutputAppender<plog::TxtFormatter> consoleAppender; // Create the 2nd appender.
	plog::init(logLevel, &fileAppender).addAppender(&consoleAppender); // Initialize the logger with the both appenders.



}