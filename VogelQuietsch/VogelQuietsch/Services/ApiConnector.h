#pragma once
#define CURL_STATICLIB
#include <wx/wx.h>
#include <wx/protocol/http.h>
#include <wx/webrequest.h>
#include <wx/event.h>

#include <curl/curl.h>
#include <wx/mstream.h>
#include <JSON/jsonreader.h>
#include <json/json.h>
#include <string>
#include <iostream>
#include <ConverterHelper.h>
#include <StorageService.h>




class ApiConnector 
{
public:
	ApiConnector();
	
	wxArrayString searchXenoCanto(wxString name, wxString country, wxString location, wxString other, bool ignoreSsp, bool ignoreSspByRegion, bool loadAllPages, bool limitToRegion, bool addTagCountry, bool addTagLocation, bool addTagOther);
	bool checkUrlAvailability(wxString urlString);
	wxImage getSonoWebRequest(wxString urlString, wxEvtHandler* eventHandler);
	wxString curlData(wxString urlString);
	wxString xenoCantoUrl;
	Json::Value getRecordings(wxString birdName, boolean call, boolean song, boolean highQuality);
	wxImage curlImage(wxString urlString);
private:
	static size_t writeCallback(void* contents, size_t size, size_t nmemb, void* userp);
	static size_t write_data_to_wxStream(void* ptr, size_t size, size_t nmemb, void* userdata);

	void addApiParameter(wxString &apiParameters, wxString parameterKey, wxString parameterValue);
	void extractBirdNamesFromJSON(wxArrayString &birdNames, Json::Value json, bool ignoreSsp, bool ignoreSspByRegion);
	wxString xenoCantoApiBase;
	
};

