#include "StorageService.h"
#include "LoggerWrapper.h"
StorageService::StorageService()
{

}

StorageService::StorageService(wxString path)
{
	setPath(path);
}

void StorageService::setPath(wxString path)
{
	StorageService::path = path;
}

wxString StorageService::getPath()
{
	return wxString();
}

wxJSONValue StorageService::readJsonFile(wxString fileName)
{
	ConverterHelper converterHelper = ConverterHelper();
	wxArrayString stringArray = readTxtFile(fileName);

	wxString document = converterHelper.concatArrayString(stringArray, _T("\n"));

	wxJSONValue  root = converterHelper.parseWxJSON(document);

	return root;
}


wxArrayString StorageService::readTxtFile(wxString fileName)
{
	wxArrayString stringArray;
	wxTextFile tFile;
	tFile.Open(fileName);
	wxString line;
	line = tFile.GetFirstLine();
	stringArray.Add(line, 1);
	while (!tFile.Eof())
	{
		line = tFile.GetNextLine();
		stringArray.Add(line, 1);
	}
	tFile.Close();
	return stringArray;
}
void StorageService::appendJsonToTxtFile(wxString fileName, Json::Value jsonValue)
{
	PLOGI << "Append json to " << fileName;
	Json::StreamWriterBuilder builder;
	builder["indentation"] = ""; // If you want whitespace-less output
	const std::string output = Json::writeString(builder, jsonValue);
	wxString wxOutput(output);
	StorageService().writeTxtFile("lastSession.json", wxOutput);
	wxTextFile file(fileName);
	boolean newFile(false);
	if (!file.Exists()) {
		file.Create();
		newFile = true;
	}
	file.Open();
	if (newFile)
	{
		file.AddLine("[");
		file.AddLine(wxOutput);
		file.AddLine("]");
	}
	else {
		file.InsertLine("," + wxOutput, file.GetLineCount() - 1);
	}
	file.Write();
	file.Close();
}
void StorageService::writeTxtFile(wxString fileName, wxArrayString stringArray)
{
	PLOGI << "Writing wxArrayString with size " << stringArray.size() << " to " << fileName;
	wxTextFile file(fileName);
	if (file.Exists()) {
		wxRemoveFile(fileName);
		PLOGW << "File overwritten!";
	}

	if (!file.Create()) {
		PLOGE << "Could not create file!";
	}

	if (!file.Open()) {
		PLOGE << "Could not open file!";
	}

	for (wxString str : stringArray) {
		PLOGD << "Writing line: " << str;
		file.AddLine(str);
	}
	PLOGD << "Writing file";
	if (!file.Write()) {
		PLOGE << "Could not write file!";
	}
	if (!file.Close()){
		PLOGE << "Could not Close file!";
	}

	
	
}

void StorageService::writeTxtFile(wxString fileName, wxString string)
{
	PLOGI << "Writing wxString to " << fileName;
	wxTextFile file(fileName);
	if (file.Exists()) {
		wxRemoveFile(fileName);
		PLOGW << "File overwritten!";
	}
	

	if (!file.Create()) {
		PLOGE << "Could not create file!";
	}

	if (!file.Open()) {
		PLOGE << "Could not open file!";
	}
	file.AddLine(string);
	PLOGD << "Writing file";
	if (!file.Write()) {
		PLOGE << "Could not write file!";
	}
	if (!file.Close()) {
		PLOGE << "Could not Close file!";
	}
}
