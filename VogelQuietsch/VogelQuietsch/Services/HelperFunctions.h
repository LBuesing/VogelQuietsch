#include <wx/listctrl.h>
#include <regex>
#include <iostream>
#include <algorithm>
class HelperFunctions
{
public:
	HelperFunctions();
	static void initListCtrl(wxListCtrl* target);
	static void fillList(wxArrayString arrayString, wxListCtrl* owner);
	static void SyncLists(wxListCtrl* fromList, wxListCtrl* toList);
	static wxArrayString GetListEntries(wxListCtrl* fromList);
	static void regexBirdList(wxListCtrl* birdListCtrl, wxArrayString birdList, wxString regex);
};
