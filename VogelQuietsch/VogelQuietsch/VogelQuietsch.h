#pragma once
#pragma warning(disable:4996)
#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/icon.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/stattext.h>
#include <wx/checkbox.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/statbmp.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/choice.h>
#include <wx/slider.h>
#include <wx/statbmp.h>
#include <wx/listbox.h>
#include <wx/listctrl.h>
#include <wx/panel.h>
#include <wx/dataview.h>
#include <wx/notebook.h>
#include <wx/frame.h>
#include <wx/dnd.h>
#include "Services/ApiConnector.h"
#include "Services/StorageService.h"
#include <wx/filedlg.h>
#include <windows.h>
#include <sys/stat.h>
#include "Services/MediaCtrlWrapper.h"
#include "Services/LoggerWrapper.h"
#include "Services/HelperFunctions.h"
#include "Services/VogelQuietschGame.h"
#include <string>
#include <fstream>
#include <sqlite3.h>
#include "Services/SQLiteWrapper.h"



///////////////////////////////////////////////////////////////////////////////
/// Class VogelQuietsch
///////////////////////////////////////////////////////////////////////////////
class VogelQuietsch : public wxFrame
{
private:
	void bindGuiElements();
	
	void searchXenoCanto(wxCommandEvent& e);
	void onDragInitSearchToEdit(wxListEvent& event);
	void onSelectSearchToEdit(wxListEvent& event);
	void onDeselectSearchToEdit(wxListEvent& event);
	void onDragInitInactive(wxListEvent& event);
	void onSelectInactive(wxListEvent& event);
	void onDeselectInactive(wxListEvent& event);
	void onDragInitActive(wxListEvent& event);
	void onSelectActive(wxListEvent& event);
	void onDeselectActive(wxListEvent& event);
	void onLoadBirdListToInactive(wxCommandEvent& event);
	void onLoadBirdListToEdit(wxCommandEvent& event);
	void onSaveBirdListFromEdit(wxCommandEvent& evt);
	wxArrayString searchResultsSelection;
	wxArrayString inactiveSelection;
	wxArrayString activeSelection;
	MediaCtrlWrapper* mediaCtrlWrapper;
	LoggerWrapper* logger;

	VogelQuietschGame* vogelQuietschGame;
protected:
	wxMenuBar* m_menubarMain;
	wxMenu* m_menuOptions;
	wxStaticText* m_staticText_QuizzOptions;
	wxCheckBox* m_checkBox_Song;
	wxCheckBox* m_checkBox_Call;
	wxCheckBox* m_checkBox_HighQuality;
	wxCheckBox* m_checkBox_Sono;
	wxCheckBox* m_checkBox_IgnoreTags;
	wxCheckBox* m_checkBox_LimitToRegion;
	wxButton* m_button_MapQuizz;
	wxStaticText* m_staticText34;
	wxTextCtrl* m_textCtrl_AudioTimeLimitLower;
	wxStaticText* m_staticText35;
	wxTextCtrl* m_textCtrl_AudioTimeLimitUpper;
	wxStaticText* m_staticText36;
	wxChoice* m_choice_MoveToInactive;
	wxStaticText* m_staticText37;
	wxStaticText* m_staticText38;
	wxChoice* m_choice_NumberOfTries;
	wxCheckBox* m_checkBox_Translate;
	wxButton* m_button_Play;
	wxButton* m_button_Pause;
	wxButton* m_button_Stop;
	wxSlider* m_slider_Time;
	wxStaticText* m_staticText_Time;
	wxStaticText* m_staticText30;
	wxSlider* m_slider_Volume;
	wxStaticText* m_staticText_Volume;
	wxStaticBitmap* m_bitmap_Sono;
	wxStaticText* m_staticText39;
	wxTextCtrl* m_textCtrl_BirdSelection;
	wxListCtrl* m_listBox_BirdSelection;
	wxButton* m_button_StartQuizz;
	wxButton* m_button_ConfirmSelection;
	wxStaticText* m_staticText_QuizzInfo;
	wxStaticText* m_staticText_QuizzInfo2;
	wxButton* m_button_DontKnow;
	wxButton* m_button_ShowBirdOnDuckDuckGo;
	wxButton* m_button_ShowRecOnXenoCanto;
	wxStaticText* m_staticText_PlaceHolder;
	wxButton* m_button_StopQuizz;
	wxStaticText* m_staticText42;
	wxStaticText* m_staticText_BirdInfoID;
	wxStaticText* m_staticText44;
	wxStaticText* m_staticText_BirdInfoGen;
	wxStaticText* m_staticText441;
	wxStaticText* m_staticText_BirdInfoSp;
	wxStaticText* m_staticText421;
	wxStaticText* m_staticText_BirdInfoSsp;
	wxStaticText* m_staticText4411;
	wxStaticText* m_staticText_BirdInfoName;
	wxStaticText* m_staticText422;
	wxStaticText* m_staticText_BirdInfoCountry;
	wxStaticText* m_staticText423;
	wxStaticText* m_staticText_BirdInfoType;
	wxStaticText* m_staticText424;
	wxStaticText* m_staticText_BirdInfoLocation;
	wxStaticText* m_staticText425;
	wxStaticText* m_staticText_BirdInfoDate;
	wxStaticText* m_staticText426;
	wxStaticText* m_staticText_BirdInfoTime;
	wxStaticText* m_staticText427;
	wxStaticText* m_staticText_BirdInfoBirdSeen;
	wxStaticText* m_staticText428;
	wxStaticText* m_staticText_BirdInfoUsedPlayback;
	wxStaticText* m_staticText429;
	wxStaticText* m_staticText_BirdInfoRec;
	wxStaticText* m_staticText4210;
	wxStaticText* m_staticText_BirdInfoQuality;
	wxStaticText* m_staticText4211;
	wxStaticText* m_staticText_BirdInfoNumRec;
	wxStaticText* m_staticText4212;
	wxStaticText* m_staticText_BirdInfoOthers;
	wxNotebook* m_notebook;
	wxPanel* m_panel_ActiveInactive;
	wxButton* m_button_LoadList;
	wxButton* m_button_LoadProfile;
	wxButton* m_button_CreateProfile;
	wxButton* m_button_SortLists;
	wxStaticText* m_staticText5;
	wxStaticText* m_staticText6;
	wxStaticText* m_staticText_LoadedProfile;
	wxStaticText* m_staticText_LoadedList;
	wxStaticText* m_staticText3;
	wxStaticText* m_staticText4;
	wxListCtrl* m_listBox_ActiveList;
	wxListCtrl* m_listBox_InactiveList;
	wxPanel* m_panel_ListCreateEdit;
	wxStaticText* m_staticText21;
	wxStaticText* m_staticText22;
	wxStaticText* m_staticText23;
	wxStaticText* m_staticText24;
	wxTextCtrl* m_textCtrl_SearchParameterName;
	wxTextCtrl* m_textCtrl_SearchParameterCountry;
	wxTextCtrl* m_textCtrl_SearchParameterLocation;
	wxTextCtrl* m_textCtrl_SearchParameterOther;
	wxStaticText* m_staticText25;
	wxCheckBox* m_checkBox_AddTagCountry;
	wxCheckBox* m_checkBox_AddTagLocation;
	wxCheckBox* m_checkBox_AddTagOther;
	wxCheckBox* m_checkBox_SearchParameterIgnoreSsp;
	wxCheckBox* m_checkBox_SearchParameterIgnoreSspByRegion;
	wxCheckBox* m_checkBox_SearchParameterLoadAllPages;
	wxCheckBox* m_checkBox_SearchParameterLimitToRegion;
	wxButton* m_button_SearchParameterMap;
	wxButton* m_button_SearchXenoCantoDb;
	wxStaticText* m_staticText28;
	wxStaticText* m_staticText_SearchResults;
	wxStaticText* m_staticText_LoadedListEdit;
	wxListCtrl* m_listBox_SearchResults;
	wxListCtrl* m_listBox_LoadedList;
	wxButton* m_button_LoadListEdit;
	wxButton* m_button_SortListEdit;
	wxButton* m_button_RemoveCollectionEdit;
	wxButton* m_button_SaveListEdit;
	wxPanel* m_panel_Statistics;
	wxDataViewListCtrl* m_dataViewListCtrl2;
	wxPanel* m_panel_History;
	wxDataViewListCtrl* m_dataViewListCtrl1;
	wxPanel* m_panel_Dictionary;
	
public:

	VogelQuietsch(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("VogelQuietsch-Quizz"), const wxPoint& pos = wxPoint(0,0), const wxSize& size = wxSize(1400, 800), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL);
	//void OnSelect(wxCommandEvent& event);
	
	~VogelQuietsch();

};
class MyTextDropTarget : public wxTextDropTarget
{
public:
	MyTextDropTarget(wxListCtrl* owner);

	virtual bool OnDropText(wxCoord x, wxCoord y,
		const wxString& data);

	wxListCtrl* m_owner;
	
};

class MyTextDropTargetActiveInactive : public wxTextDropTarget
{
public:
	MyTextDropTargetActiveInactive(wxListCtrl* target, wxListCtrl* source);

	virtual bool OnDropText(wxCoord x, wxCoord y,
		const wxString& data);

	wxListCtrl* m_target;
	wxListCtrl* m_source;

};
/*
MMMMMMMMMMMMW0xolld0NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMXc     .;kNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMWXOk0KNNkll;.   .kMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMXc. ..;OMMM0,   .kMMMMMMMMMMMMMWNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMXl.    .k0dc'  .;xNMMMMMMMMMMMMXo'cKMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMWd.    .lo.   ,d0WMMMMMMMMMMMMMM0' .oNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMWk'    .dk,  .dXMMMMMMMMMMMMMMMMNO;  '0MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNNXKNNWXKK0KNKO0WMM
MMK:    'xXd. .xWMMMMMMMMMMMMMMNOlcc,. .dWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXXWNOo;,;'.,;:,...';..cXMM
MWx.   'kWNOkxONMMMMMMMMMMMMMMKc':xK0c..;0MMMMMMWNXK0O0NMMMMMMMMMMMMW00XOc.,c:....    .''..    .lXMM
MNl.  'kW0:.,OMM0ooOXWMMMMMMMXc'cokXXKko;dWMWN0d:''..'oXMMMMMMMMMMMNx'.,;,..:xdcxOodxxOXXKd, .,lONMM
MWKxdoOWMXo:dXMMKl..;odkXWMMWd'oKXXNNXNO;lN0c,..,ldxOKWMMMMMMMMWWNk:,,,l0KxONMWWWMMMMMMMWXl..,xNMMMM
N0xkKWMMMMMWMMMMMWk;:xxl:cxXNl,kNNXXNKOl.'c;..,:cllxXMMMMMMMMMKo;;,lKNNWNNWMMMMXO0WMMMMMXo. .dNMMMMM
c.  :XMMMMMMMMMMMMWKl,dXKxl:c'.dXXKko:,:lxOOkOXX0l''dWMMMMMMNx;;;:kWMMMNd:xXMMMNxcxNMMMMKo.'xNMMMMMM
l'.;kNMMMMMMMMMMMMMMNd;c0WWKo'.,c:;:lxOKXXXxd0XXO:.,kMMMMNkl;,oXWWMMMMMW0c'oXMMMW0kKWMMNl..lXMMMMMMM
WNXNMMMMMMMMMMMMMMMMMW0l;l0N0oxOxdkKXNXXNNX00K0d;;ckWMMW0c'.,xNMMMMMMWWMMN00NMMMMMMMMM0c.'xNMMMMMMMM
MMMMMMMWKxoodddooddxkO00d;,lOWMMMMWNNNXXXXXKxc,',xXNWN0l;cOXNWWXXWMMWOo0MMMMWNWMMMMMN0:.'dNMMMMMMMMM
MMMMMMMXo'''',:cdkkxddoool;';OWMMMMMWWNNNXXkl;'...;:l:;lONMMMMNo;kWMMXocOWMMXlcKMMMNo. .oNMMMMMMMMMM
MMMMMMMWNXK0OxdocloxOXWWWNXXXNWMMMMMMMMMWWWWNOo;,,,.,xXWMMMMMMMXdcdXWMNxl0WMW0lxWMNk,.:kNMMMMMMMMMMM
MMMMMMMMMMMMMMMWN0kdlcld0WMMMMMMMMMMMMMMMMMMMMWNNXKKXMMMMMMMMMMMWKOXWMMWNNMMMMNNNk:'cOWMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMWXklccdONMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWk'.lkO0NWWMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMWN0o;:OWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXl.,,...,oXMMMMMMMMMM
MMMMMMMMMMMMMMMMMMXxdddddkk;'kWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMXxxNMMMWXKOl.  .cKMMMMMMMMMM
MMMMMMMMMMMMMMMNOo;:dkOkoc:l0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKKWMW0okWMMK:lNMMMMMMXc..'l0WMMMMMMMMMM
MMMMMMMMMMMMNOo::oONMMMMMWNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNolNMK::0MMMO;xWMMMMMW0c...lKMMMMMMMMMMM
MMMMMMMMMWXx:;lONMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0KWMOoOWMWk;cKMMMMMWO;..c0WMMMMMMMMMMMM
MMMMMMMMXo;:xKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMXdxNMMMMMNx,.'cxNMMMMMMMMMMMMM
MMMMMMW0:,xNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMNOo:;:kWMMMMMMMMMMMMM
MMMMMW0;;OWMMMMMMMMMMMMMMMMMMMMMMMMWXNMMMMMMMWX0kxxdod0WWWWWWMMMMMMMMMMMMMMMMMMMXd;',kWMMMMMMMMMMMMM
MMMMWk,;0WMMMMMMMMMMMMMMMMMMMMMMMMMO;lXMMN0dl:,.......,c:cddookNMMMMWN0xOXWMMNOxo,.'xWMMMMMMMMMMMMMM
MMMMO,;0MMMMMMMMMMMMMMMMMMMMMMMMMMK:..oko;'.''....',;;;'.lKN0o:coddolccccclxxc..cxOKNMMMMMMMMMMMMMMM
MMMNl,kWMMMMMMMMMMMMMMMMMMMMMMMMMNo,ol:,..;c:.';.'::cc:',OMMMWN0OkkO0KWMWKkxxxxONMMMMMMMMMMMMMMMMMMM
MMMK:cXMMMMMMMMMMMMMMMMMMMMMMMMMMK;cXWXc.,cc,.dd..'',''.;KMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMWd,xWMMMMMMMMMMMMMMMMMMMMMMMMMWd,xWMk'..''.;0O'.......:XMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MM0;cXMMMMMMMMMMMMMMMMMMMMMMMMMMO,cXMXc.,;'..oN0,.',,::.:XMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMk,dWMMMMMMMMMMMMMMMMMMMMMMMMMNc,OWMk'.:c:.:XM0,.',,;;.;KMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMk,oNMMMMMMMMMMMMMMMMMMMMMMMMWk,oNMXc.;c:.'OMM0,.'',;;.:XMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMK;;0MMMMMMMMMMMMMMMMMMMMMMMXo,lKMWx..:c,.lNMM0,..':l:.cNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMWO::oxKMMMMMMMMMMMMMMMMMMW0:,xNMMK:.;::'.dWMMK:..':c:.:XMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMN0d'cXMMMMMMMMMMMMMMWKkl;c0WMMNo.':;,..xWMMNl...;::.'OMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMNl'kMMMMMMMMMMWXOdc:cd0NMMMWk''::,,..xWMMMk. .,:l,.oWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMO::oddONWNKOxl:cokXWMMMMMNk,.,lc,,..xWMMMK; .'::'.:XMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMWNKOk:':lloooxKNMMMMMMMMWk. ..';,..:KMMMMWd'.......xWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMKkkKNWMMMMMMMMMMMMNx,........;0MMMMMMKd;..',,.'dKWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMWX0xl;..,;;;;,.'xWMMMMMMWNd.';;;,'.,:codxkkOKNWMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMWX0kdl,...',;;;;;'.;OWMMMMMMMMMk..;;;;;;,,'''''''';kWMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMXxc:,...',;;;,,'',;,.,0MMMMMMMMMMMk'.;,.';;;:::;;,,,,:OWMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMNOdlc:;,,'''',,...'.,kWMMMMMMMMMMMO'.,.'xNNNNWNNXKKKNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMWNXK0OOkOd;..'.'kWMMMMMMMMMMMMK:...;0MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMWk:,;,;kWMMMMMMMMMMMMMMXkxx0NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNX00XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

*/